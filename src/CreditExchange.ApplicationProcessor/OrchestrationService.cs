﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.CompanyDb.Abstractions;
using CreditExchange.GeoProfile;
using CreditExchange.Syndication.Lenddo;
using LendFoundry.DataAttributes;
using Newtonsoft.Json.Linq;
using Gender = CreditExchange.Applicant.Gender;
using System.Globalization;
using System.Threading;
using CreditExchange.Applicant;
using CreditExchange.Cibil;
using CreditExchange.Lenddo.Abstractions;
using CreditExchange.Perfios;
using CreditExchange.Syndication.Cibil.Request;
using CreditExchange.Syndication.Cibil.Response;
using CreditExchange.Syndication.Perfios.Response;
using CreditExchange.Syndication.ViewDNS;
using LendFoundry.EmailVerification;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.ProductRule;
using LendFoundry.StatusManagement;
using LendFoundry.VerificationEngine;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;

namespace CreditExchange.ApplicationProcessor
{
    public class OrchestrationService : IOrchestrationService
    {
        public OrchestrationService (
            //ICrifCreditReportService crifCreditReportService
            //, ICrifPanVerificationService crifPanVerificationService
            IGeoProfileService geoProfileService, IInitialOfferEngineRepository offerEngineRepository, IDataAttributesEngine dataAttributesEngine, IFinalOfferEngineRepository finalOfferEngineRepository, IPerfiosService perfiosService, ICompanyService companyService, ILenddoService lenddoService, IEntityStatusService entityStatusService, IViewDnsService viewDnsService, ILogger logger, ITenantTime tenantTime, IEmailVerificationService emailVerificationService, IVerificationEngineService verificationEngineService, ApplicationProcessorConfiguration applicationProcessorConfiguration, ICibilReportService cibilReportService,
            Application.IApplicationService applicationService, IApplicantService applicantService, IProductRuleService productRuleService) //,IApplicationService applicationService)
        {
            DataAttributesEngine = dataAttributesEngine;
            OfferEngineRepository = offerEngineRepository;
            GeoProfileService = geoProfileService;
            FinalOfferEngineRepository = finalOfferEngineRepository;
            PerfiosService = perfiosService;
            CompanyService = companyService;
            LenddoService = lenddoService;
            EntityStatusService = entityStatusService;
            ViewDnsService = viewDnsService;
            Logger = logger;
            EmailVerificationService = emailVerificationService;
            TenantTime = tenantTime;
            VerificationEngineService = verificationEngineService;
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
            CibilReportService = cibilReportService;
            ExternalApplicationService = applicationService;
            ApplicantService = applicantService;
            ProductRuleService = productRuleService;
        }

        #region "Private Member"
        private ApplicationProcessorConfiguration ApplicationProcessorConfiguration { get; }
        private IInitialOfferEngineRepository OfferEngineRepository { get; }
        private IGeoProfileService GeoProfileService { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private IFinalOfferEngineRepository FinalOfferEngineRepository { get; }
        private IPerfiosService PerfiosService { get; }
        private ILenddoService LenddoService { get; }
        private ICompanyService CompanyService { get; }

        private IEntityStatusService EntityStatusService { get; }
        private IViewDnsService ViewDnsService { get; }
        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }
        private IEmailVerificationService EmailVerificationService { get; }
        private IVerificationEngineService VerificationEngineService { get; }

        // private IApplicationService ApplicationService { get; }
        private ICibilReportService CibilReportService { get; }
        private Application.IApplicationService ExternalApplicationService { get; }
        #endregion
        private IApplicantService ApplicantService { get; }
        private IProductRuleService ProductRuleService { get; }
        public async Task<IFinalOfferDefination> RunFinalOffer (string applicationNumber, bool isReCallSyndication, string productId, string workflowStatusId)
        {
            var isManualReview = false;

            var finalOfferResult = new FinalOfferDefination
            {
                EntityId = applicationNumber,
                EntityType = "application"
            };
            try
            {
                var applicantInformation = await GetApplicationInformation (applicationNumber);

                var perfiosReferenceNumber = CallPerfiosCreditReportSyndication (applicationNumber, isReCallSyndication).Result;

                //var companyDbResponse = CallCompanyDbSyndication(applicationNumber, applicantInformation.CinNumber);
                try
                {
                    var lendoResponse = CallLenddoSyndication (applicationNumber);
                }
                catch (Exception ex)
                {
                    Logger.Error (ex.Message);
                }

                JArray perfiosReport = null;
                JArray manualCashFlow = null;
                manualCashFlow = (JArray) DataAttributesEngine.GetAttribute ("application", applicationNumber, "manualCashFlowReport").Result;
                FaultRetry.RunWithAlwaysRetry (() =>
                {
                    perfiosReport = (JArray) DataAttributesEngine.GetAttribute ("application", applicationNumber, "perfiosReport").Result;
                    if ((perfiosReport == null || perfiosReport.Count <= 0) && (manualCashFlow == null || manualCashFlow.Count <= 0))
                        throw new InvalidOperationException ("perfiosReport/manualCashFlowReport not found");
                }, 5, 4);

                if ((perfiosReport == null || perfiosReport.Count <= 0) && (manualCashFlow == null || manualCashFlow.Count <= 0))
                    throw new InvalidOperationException ("perfiosReport/manualCashFlowReport not found");

                VerifyRule (applicationNumber, productId, "ScoringCritiria", "ScoringRules", "FinalOfferScoreCalculation");

                var finalOfferScoreRuleResult = VerifyRule (applicationNumber, productId, "OfferCritiria", "FinalOfferCalculation", "FinalOfferCalculation");

                var isEligible = finalOfferScoreRuleResult.Result;

                if (isEligible == Result.Passed)
                {
                    var intermidiateResult =
                        JObject.FromObject (finalOfferScoreRuleResult.IntermediateData)
                        .ToObject<Dictionary<string, object>> ();
                    var finalOffer = JArray.FromObject (intermidiateResult["offerData"]).ToObject<List<FinalOffer>> ();
                    finalOfferResult.FinalOffers = finalOffer.ConvertAll (o => (IFinalOffer) o);
                    finalOfferResult.GeneratedOn = new TimeBucket (TenantTime.Now);

                    foreach (var offer in finalOffer)
                    {
                        isManualReview = offer.ManualReview;
                        offer.OfferId = Guid.NewGuid ().ToString ("N");
                    }
                    finalOfferResult.Status = ScoreCardApplicationStatus.Completed;

                    var workEmailStatus = await VerificationEngineService.GetStatus ("application", applicationNumber, workflowStatusId, "WorkEmailVerification");
                    if (workEmailStatus != null && workEmailStatus.VerificationStatus != VerificationStatus.Passed)
                    {
                        finalOfferResult.Status = ScoreCardApplicationStatus.Completed;
                        finalOfferResult.Reasons = new List<string> { "Work Email Verification Failed" };
                    }
                }
                else
                {
                    finalOfferResult.Status = ScoreCardApplicationStatus.Rejected;
                    finalOfferResult.Reasons = finalOfferScoreRuleResult.ResultDetail;
                }
                if (isManualReview)
                {
                    finalOfferResult.Status = ScoreCardApplicationStatus.ManualReview;
                    finalOfferResult.Reasons = finalOfferScoreRuleResult.ResultDetail;
                }
            }
            catch (OfferGenerationFailedException exception)
            {
                finalOfferResult.Status = ScoreCardApplicationStatus.Failed;
                finalOfferResult.Reasons = new List<string> { exception.Message };
            }
            catch (Exception exception)
            {
                finalOfferResult.Status = ScoreCardApplicationStatus.Failed;
                finalOfferResult.Reasons = new List<string> () { exception.Message };
            }
            finalOfferResult.IsAvailable = true;
            await FinalOfferEngineRepository.AddOffer ("application", applicationNumber, finalOfferResult);
            //FinalOfferEngineRepository.Add(finalOfferResult);
            //TODO: What about reason
            //ToDo : Rules will return the reason code which will match with status management

            return finalOfferResult;
        }

        public async Task<IInitialOfferDefination> RunInitialOffer (string applicationNumber, bool isReCallSyndication, string productId)
        {
            Logger.Info ($"[OrchestrationService] RunInitialOffer method  execution started for [{applicationNumber}]");
            var initialOfferResult = new InitialOfferDefination
            {
                EntityId = applicationNumber,
                EntityType = "application"
            };

            try
            {
                Logger.Info ($"[OrchestrationService/RunInitialOffer] GetApplicationInformation execution started for [{applicationNumber}]");
                var applicantInformation = await GetApplicationInformation (applicationNumber);
                Logger.Info ($"[OrchestrationService/RunInitialOffer] GetApplicationInformation execution finished for [{applicationNumber}] result :  [{(applicantInformation != null ? JsonConvert.SerializeObject(applicantInformation) : null)}] ");
                //  switch product
                var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
                var productDetail = await ProductRuleService.RunRule ("application", applicationNumber, activeWorkflow.ProductId, "SwitchProductDecision", applicantInformation);
                var intermidiateResultProduct =
                    JObject.FromObject (productDetail.IntermediateData)
                    .ToObject<Dictionary<string, object>> ();
                object ruleProductId = null;
                object switchProductId = null;
                if (intermidiateResultProduct.TryGetValue ("productId", out ruleProductId))
                    switchProductId = ruleProductId;
                if (productId != Convert.ToString (switchProductId))
                {
                    if (!string.IsNullOrWhiteSpace (switchProductId.ToString ()))
                    {
                        productId = Convert.ToString (switchProductId);
                        await EntityStatusService.ProcessStatusWorkFlow ("application", applicationNumber, activeWorkflow.ProductId, activeWorkflow.StatusWorkFlowId, WorkFlowStatus.Completed);
                        await ExternalApplicationService.SwitchProduct (applicationNumber, switchProductId.ToString ());
                    }
                }
                // Initiate manual verification of Employment at the time of initial offer selected

                Logger.Info ($"[OrchestrationService/RunInitialOffer] VerifyRule EligibilityCriteria execution started for [{applicationNumber}]");
                VerifyRule (applicationNumber, productId, "EligibilityCriteria", "Prequalification", "ScoreCardEligibilityRule");
                Logger.Info ($"[OrchestrationService/RunInitialOffer] VerifyRule EligibilityCriteria execution finished for [{applicationNumber}]");
                var primaryBuerua = ApplicationProcessorConfiguration.PrimaryCreditBureau;

                primaryBuerua = string.IsNullOrEmpty (primaryBuerua) ? "CIBIL" : primaryBuerua;

                var crifCreditReportReferenceNumber = CallCreditReportSyndication (applicationNumber,
                    applicantInformation, primaryBuerua, isReCallSyndication, productId);

                //VerifyPan(applicationNumber, applicantInformation, isReCallSyndication);

                var geoProfileReferenceNumber = CallGeoProfileSyndication (applicationNumber, applicantInformation.ZipCode, isReCallSyndication);

                Dictionary<string, string> referenceNumbers = new Dictionary<string, string> ();
                // TODO : Temporary change to check 
                //JArray cibilReport = null;
                //FaultRetry.RunWithAlwaysRetry(() =>
                //{
                //    cibilReport = (JArray)DataAttributesEngine.GetAttribute("application", applicationNumber, "cibilReport").Result;
                //    if (cibilReport == null || cibilReport.Count <= 0)
                //        throw new InvalidOperationException("cibilReport not found");
                //});
                //if (cibilReport == null || cibilReport.Count <= 0)
                //    throw new InvalidOperationException("cibilReport not found");

                // CompanyDb Verification
                try
                {
                    CallCompanyDbSyndication (applicationNumber, applicantInformation.CinNumber, isReCallSyndication);
                }
                catch (Exception ex)
                {
                    Logger.Info (ex.Message);
                }

                Dictionary<string, string> referenceNumber = new Dictionary<string, string> ();
                //if (!string.IsNullOrEmpty(crifCreditReportReferenceNumber))
                //{
                //    referenceNumber.Add("crifCreditReport", crifCreditReportReferenceNumber);
                //}
                JArray cibilReport = null;
                FaultRetry.RunWithAlwaysRetry (() =>
                {
                    cibilReport = (JArray) DataAttributesEngine.GetAttribute ("application", applicationNumber, "cibilReport").Result;
                    if (cibilReport == null || cibilReport.Count <= 0)
                        throw new InvalidOperationException ("cibilReport not found");
                });
                if (cibilReport == null || cibilReport.Count <= 0)
                    throw new InvalidOperationException ("cibilReport not found");

                VerifyGeoProfile (applicationNumber, productId);
                var initialOfferRuleResult = VerifyRule (applicationNumber, productId, "OfferCritiria",
                    "InitialOfferCalculation", "CalculateInitialOffer");

                var intermidiateResult =
                    JObject.FromObject (initialOfferRuleResult.IntermediateData)
                    .ToObject<Dictionary<string, object>> ();

                var initialOffer =
                    JArray.FromObject (intermidiateResult["offerData"]).ToObject<List<InitialOffer>> ();

                foreach (var offer in initialOffer)
                {
                    offer.OfferId = Guid.NewGuid ().ToString ("N");
                }

                initialOfferResult.Offers = initialOffer.ConvertAll (o => (IInitialOffer) o);
                initialOfferResult.Status = ScoreCardApplicationStatus.Completed;
                initialOfferResult.GeneratedOn = new TimeBucket (TenantTime.Now);

                if (ApplicationProcessorConfiguration.EnableEmail)
                {
                    try
                    {
                        CallEmailVerificationSyndication (applicationNumber, applicantInformation.PersonalEmail, applicantInformation.FirstName);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error (ex.Message);
                    }
                }
                try
                {
                    CallViewDnsSyndication (applicationNumber, applicantInformation.CompanyEmailAddress, applicantInformation.CompanyName, applicantInformation.CinNumber, isReCallSyndication);
                }
                catch (Exception ex)
                {
                    Logger.Error (ex.Message);
                }
                initialOfferResult.IsAvailable = true;
            }
            catch (OfferGenerationFailedException exception)
            {
                initialOfferResult.Status = ScoreCardApplicationStatus.Failed;
                initialOfferResult.Reasons = new List<string> { exception.Message };
            }
            catch (AggregateException exception)
            {
                initialOfferResult.Status = ScoreCardApplicationStatus.Failed;
                initialOfferResult.Reasons = new List<string> () { exception.InnerException.Message };
            }
            catch (Exception exception)
            {
                initialOfferResult.Status = ScoreCardApplicationStatus.Failed;
                initialOfferResult.Reasons = new List<string> () { exception.Message };
            }
            initialOfferResult.ProductId = productId;
            var newActiveWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            initialOfferResult.StatusWorkFlowId = newActiveWorkflow?.StatusWorkFlowId;
            await OfferEngineRepository.AddOffer ("application", applicationNumber, initialOfferResult);
            Logger.Info ($"[OrchestrationService] RunInitialOffer method  execution finished for [{applicationNumber}]");
            return initialOfferResult;
        }

        /// <summary>
        /// RepullCibil 
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<bool> RepullCibil (string applicationNumber, string productId)
        {
            var flag = false;
            Logger.Debug ($"[OrchestrationService] RepullCibil method  execution started for [{applicationNumber}]");
            Logger.Debug ($"[OrchestrationService/RunInitialOffer] GetApplicationInformation execution started for [{applicationNumber}]");
            var applicantInformation = await GetApplicationInformation (applicationNumber);
            Logger.Debug ($"[OrchestrationService/RunInitialOffer] GetApplicationInformation execution finished for [{applicationNumber}] result :  [{(applicantInformation != null ? JsonConvert.SerializeObject(applicantInformation) : null)}] ");

            try
            {
                Logger.Debug ($"Syndication servive call started [CallCibilCreditReportSyndication] in RepullCibil method  execution started for [{applicationNumber}]");
                //// TODOs
                CallCibilCreditReportSyndication (applicationNumber, applicantInformation, productId);
                flag = true;
            }
            catch (Exception ex)
            {
                Logger.Error (ex.Message);
            }

            return flag;
        }

        #region Pan Verification

        //private IPanVerificationResponse CallPanVerificationSyndication(string applicationNumber,
        //    IApplicantInformation applicantInformation)
        //{
        //    var objPanVerification = new PanVerificationRequest
        //    {
        //        Dob = applicantInformation.DateOfBirth.LocalDateTime.ToString(),
        //        Gender =
        //            applicantInformation.Gender == Gender.Male
        //                ? Syndication.Crif.Gender.Male.ToString()
        //                : Syndication.Crif.Gender.Female.ToString(),
        //        Name = applicantInformation.FirstName,
        //        Pan = applicantInformation.Pan
        //    };

        //    IPanVerificationResponse panVerificationResponse = null;

        //    try
        //    {
        //        FaultRetry.RunWithAlwaysRetry(() =>
        //        {
        //            panVerificationResponse =
        //                CrifPanVerificationService.VerifyPan("application", applicationNumber, objPanVerification).Result;
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.Message + " Stack Trace: " + ex.StackTrace);
        //    }
        //    return panVerificationResponse;
        //}

        //private void VerifyPan(string applicationNumber, IApplicantInformation applicantInformation, bool isReCallSyndication)
        //{
        //    var count = 1;
        //    const int maxAttempt = 3;
        //    dynamic attributes = null;
        //    try
        //    {
        //        attributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "cibilReport").Result;
        //        if (attributes != null)
        //        {
        //            var panVerificationStatus = VerificationEngineService.GetStatus("application", applicationNumber, "PanVerification").Result;

        //            if (panVerificationStatus.VerificationStatus == VerificationStatus.Failed && (string.IsNullOrEmpty(JObject.Parse(attributes.ToString()).panNameMatch.Value) || string.IsNullOrEmpty(JObject.Parse(attributes.ToString()).panNoMatch.Value)))
        //            {
        //                VerificationEngineService.InitiateManualVerification("application", applicationNumber, "PanVerification").Wait();
        //                return;
        //            }
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        Logger.Error("Error Occured while execute" + ex.StackTrace);
        //    }
        //    FaultRetry.RunWithAlwaysRetry(() =>
        //    {
        //        var panVerificationStatus = VerificationEngineService.GetStatus("application", applicationNumber, "PanVerification").Result;
        //        if (panVerificationStatus == null)
        //        {
        //            if (count < maxAttempt)
        //            {
        //                count++;
        //                throw new Exception("Pan verification Failed");
        //            }
        //        }               
        //    });
        //}

        //Dictionary<string, object> attributes = null;
        //if (!isReCallSyndication)
        //{
        //    attributes = DataAttributesEngine.GetAttribute("application", applicationNumber,
        //      new List<string> { "crifPanReport" }).Result;
        //}
        //Dictionary<string, string> referenceNumbers = null;
        //if (attributes == null || attributes.Count <= 0)
        //{
        //    var syndicationResult = CallPanVerificationSyndication(applicationNumber, applicantInformation);

        //    if (syndicationResult == null)
        //        throw new OfferGenerationFailedException("Unable to get syndication response for PanVerification");
        //    referenceNumbers = new Dictionary<string, string>
        //{
        //    {"crifPanReport", syndicationResult.ReferenceNumber}
        //};
        //}

        //VerifyRule(applicationNumber, "EligibilityCriteria", "Prequalification", "CrifPanVerificationRule",
        //    referenceNumbers);

        #endregion

        #region Crif Credit Report

        private string CallCreditReportSyndication (string applicationNumber,
            IApplicantInformation applicantInformation, string bureauType, bool isReCallSyndication, string productId)
        {
            Logger.Info ($"[OrchestrationService/RunInitialOffer] CallCreditReportSyndication method execution started for [{applicationNumber}] ");
            string referenceNumber = string.Empty;
            JArray attributes = null;
            //  object attributes = null;
            if (bureauType == "CIBIL")
            {
                if (!isReCallSyndication)
                {

                    attributes = (JArray) DataAttributesEngine.GetAttribute ("application", applicationNumber, "cibilReport").Result;

                }

                if (attributes == null || attributes.Count <= 0)
                {
                    var cibilReport = CallCibilCreditReportSyndication (applicationNumber, applicantInformation, productId);
                }
            }
            Logger.Info ($"[OfferEngineService/RunInitialOffer] CallCreditReportSyndication method execution finished for [{applicationNumber}] ");
            return referenceNumber;
        }

        private IReport CallCibilCreditReportSyndication (string applicationNumber, IApplicantInformation applicantInformation, string productId)
        {
            Logger.Info ($"[OrchestrationService/RunInitialOffer/CallCreditReportSyndication] CallCibilCreditReportSyndication method execution started for [{applicationNumber}] payload :[{  (applicantInformation != null ? JsonConvert.SerializeObject(applicantInformation) : null)}] ");
            IReportRequest cibilRequest = new ReportRequest ();
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            foreach (var address in applicantInformation.Addresses)
            {

                if (address.AddressType.ToString () == "Current")
                {
                    cibilRequest.Address1ResidentOccupation = applicantInformation.ResidenceType.ToString ();
                    cibilRequest.Address1Line1 = address.AddressLine1;
                    cibilRequest.Address1Line2 = address.AddressLine2 != null ? address.AddressLine2 : "";
                    cibilRequest.Address1Line3 = address.AddressLine3;
                    cibilRequest.Address1Pincode = address.PinCode;
                    cibilRequest.Address1State = address.State.ToString ();
                    cibilRequest.Address1City = address.City;
                    cibilRequest.Address1AddressCategory = address.AddressType.ToString ();
                }
            }

            if (applicantInformation.employmentAddress != null && applicantInformation.employmentAddress.Count > 0)
            {
                cibilRequest.Address2Line1 = applicantInformation.employmentAddress[0].AddressLine1;
                cibilRequest.Address2Line2 = applicantInformation.employmentAddress[0].AddressLine2;
                cibilRequest.Address2Line3 = applicantInformation.employmentAddress[0].AddressLine3;
                cibilRequest.Address2Pincode = applicantInformation.employmentAddress[0].PinCode;
                cibilRequest.Address2State = applicantInformation.employmentAddress[0].State;
                cibilRequest.Address2City = applicantInformation.employmentAddress[0].City;

            }
            cibilRequest.AdharNumber = applicantInformation.AadhaarNumber;

            cibilRequest.ApplicantFirstName = applicantInformation.FirstName;
            cibilRequest.ApplicantLastName = applicantInformation.LastName;
            cibilRequest.ApplicantMiddleName = applicantInformation.MiddleName;
            cibilRequest.DateOfBirth = applicantInformation.DateOfBirth.LocalDateTime;
            cibilRequest.EmailId = applicantInformation.PersonalEmail;

            cibilRequest.Gender = applicantInformation.Gender.ToString ();

            cibilRequest.LandLineNo = applicantInformation.LandlineNo;

            cibilRequest.MobileNo = applicantInformation.MobileNo;

            cibilRequest.LoanAmount = applicantInformation.RequestedAmount.ToString ();
            cibilRequest.OfficeLandLineNo = "";
            cibilRequest.PanNo = applicantInformation.Pan?.ToUpper ();
            cibilRequest.AdharNumber = applicantInformation.AadhaarNumber;
            cibilRequest.PassportNumber = "";
            cibilRequest.RationCardNo = "";
            cibilRequest.VoterId = "";
            cibilRequest.Lender = productId;

            IReport result = null;

            try
            {
                Logger.Info ($"[OrchestrationService/RunInitialOffer/CallCreditReportSyndication/CallCibilCreditReportSyndication]  CibilReportService GetReport method  execution started for [{applicationNumber}] cibilrequest :[{  (cibilRequest != null ? JsonConvert.SerializeObject(cibilRequest) : null)}] ");
                FaultRetry.RunWithAlwaysRetry (() =>
                {
                    result = CibilReportService.GetReport ("application", applicationNumber, cibilRequest).Result;

                });
                Logger.Info ($"[OrchestrationService/RunInitialOffer/CallCreditReportSyndication/CallCibilCreditReportSyndication]  CibilReportService GetReport method  execution finished for [{applicationNumber}] result :[{  (result != null ? JsonConvert.SerializeObject(result) : null)}] ");
            }
            catch (Exception ex)
            {
                result = null;
                Logger.Error (ex.Message + " Stack Trace:" + ex.StackTrace);
            }
            Logger.Info ($"[OrchestrationService/RunInitialOffer/CallCreditReportSyndication] CallCibilCreditReportSyndication method execution finished for [{applicationNumber}] result :[{  (result != null ? JsonConvert.SerializeObject(result) : null)}] ");
            return result;
        }

        private void VerifyCreditReportEligibility (string applicationNumber, string productId)
        {

            Logger.Info ($"[OrchestrationService/RunInitialOffer] VerifyCreditReportEligibility method execution started for [{applicationNumber}]");
            VerifyRule (applicationNumber, productId, "EligibilityCriteria", "Prequalification", "CreditEligibilityCheck");
            Logger.Info ($"[OrchestrationService/RunInitialOffer] VerifyCreditReportEligibility method execution finshed for [{applicationNumber}]");
        }

        #endregion

        #region Geo Profile

        private string CallGeoProfileSyndication (string applicationNumber, string pinCode, bool isReCallSyndication)
        {
            Logger.Info ($"[OrchestrationService/RunInitialOffer] CallGeoProfileSyndication method execution started for [{applicationNumber}] pincode :[{pinCode}]");

            string referenceNumber = string.Empty;
            JArray attributes = null;

            if (!isReCallSyndication)
            {
                attributes = (JArray) DataAttributesEngine.GetAttribute ("application", applicationNumber, "geoRiskPinCode").Result;
            }
            if (attributes == null || attributes.Count < 0)
            {
                GeoPincodeResponse result = null;
                Logger.Info ($"[OrchestrationService/RunInitialOffer/CallGeoProfileSyndication]  GeoProfileService GetPincode method execution started for [{applicationNumber}] pincode :[{pinCode}]");
                FaultRetry.RunWithAlwaysRetry (() =>
                {
                    result = GeoProfileService.GetPincode ("application", applicationNumber, pinCode).Result;
                });
                Logger.Info ($"[OrchestrationService/RunInitialOffer/CallGeoProfileSyndication] GeoProfileService GetPincode method execution finished for [{applicationNumber}] pincode :[{pinCode}]");
                if (result != null)
                    referenceNumber = result.ReferenceNumber;
            }
            Logger.Info ($"[OrchestrationService/RunInitialOffer] CallGeoProfileSyndication method execution finished for [{applicationNumber}] ");
            return referenceNumber;
        }

        private void VerifyGeoProfile (string applicationNumber, string productId)
        {
            Logger.Info ($"[OrchestrationService/RunInitialOffer] VerifyGeoProfile method execution started for [{applicationNumber}]");
            VerifyRule (applicationNumber, productId, "EligibilityCriteria", "Prequalification", "GeoRiskEligibility");
            Logger.Info ($"[OrchestrationService/RunInitialOffer] VerifyGeoProfile method execution finished for [{applicationNumber}]");
        }

        #endregion

        private async Task UpdateBankInformation (string applicationNumber, IBankInformation bankInformationRequest)
        {

            bankInformationRequest.AccountType = AccountType.Savings;
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            var status = await EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId);
            var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            if (application != null && status != null && status.Code == ApplicationProcessorConfiguration.Statuses["Approved"])
            {
                if (string.IsNullOrWhiteSpace (bankInformationRequest.BankId))
                {
                    await ApplicantService.AddBank (application.ApplicantId, bankInformationRequest);
                }
                else
                {
                    await ApplicantService.UpdateBank (application.ApplicantId, bankInformationRequest.BankId, bankInformationRequest);
                }
            }
            else
            {
                if (status != null && ApplicationProcessorConfiguration != null && !(ApplicationProcessorConfiguration.BankChangeStatus.Contains (status.Code)))
                {
                    throw new InvalidOperationException ($"Can not update bank detail when status is {status.Code}");
                }
                await ExternalApplicationService.UpdateBankInformation (applicationNumber, bankInformationRequest);
            }
        }
        private async Task<string> CallPerfiosCreditReportSyndication (string applicationNumber, bool isReCallSyndication)
        {
            string referenceNumber = string.Empty;
            object attributes = null;
            if (!isReCallSyndication)
            {
                attributes = DataAttributesEngine.GetAttribute ("application", applicationNumber, "perfiosReport").Result;

            }
            if (attributes == null)
            {

                IAnalysisReport result = null;

                FaultRetry.RunWithAlwaysRetry (() =>
                {
                    try
                    {
                        var perfiosdata = PerfiosService.RetrieveXmlReport ("application", applicationNumber).Result;
                        result = (AnalysisReport) perfiosdata;

                    }
                    catch (Exception ex)
                    {
                        Logger.Error (ex.Message);
                    }
                });
                if (result != null)
                {
                    try
                    {
                        var newActiveWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
                        referenceNumber = result.ReferenceNumber;
                        if (result?.CustomerInfo?.SourceOfData == "NetBanking Account")
                        {
                            await UpdateBankInformation (applicationNumber, new BankInformation ()
                            {
                                AccountHolderName = result.CustomerInfo.Name,
                                    AccountType = AccountType.Savings,
                                    IfscCode = result.IFSCCode,
                                    AccountNumber = result.AccountNo,
                                    BankName = result.Bank,
                                    BankAddresses = new Address ()
                                    {
                                        AddressLine1 = result.Location,
                                            AddressType = Applicant.AddressType.Bank
                                    }
                            });
                            Logger.Info ("Bank detail update for netbanking");
                            await VerificationEngineService.Verify ("application", applicationNumber, newActiveWorkflow.StatusWorkFlowId, "BankVerification",
                                "PerfiosNetBanking", "PerfiosNetBanking", (new { result = "true" }));
                            Logger.Info ("Automatic fact verification done for netbanking");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error (ex.Message);
                    }
                }

            }
            return referenceNumber;
        }
        // if perfios verification failed by syndication then we will initiate manual for the same
        private void VerifyPerfiosInformation (string applicationNumber)
        {
            var activeWorkflow = EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber).Result;
            var bankVerificationStatus = VerificationEngineService.GetStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, "BankVerification").Result;
            if (bankVerificationStatus != null && bankVerificationStatus.VerificationStatus != VerificationStatus.Passed)
            {
                VerificationEngineService.InitiateManualVerification ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, "BankVerification", "BankManual").Wait ();

            }
        }

        private string CallCompanyDbSyndication (string applicationNumber, string cinNumber, bool isReCallSyndication)
        {
            Logger.Info ($"[OrchestrationService/RunInitialOffer] CallCompanyDbSyndication method execution started for [{applicationNumber}] Cin :[{cinNumber}]");
            string referenceNumber = string.Empty;
            JArray attributes = null;
            if (!isReCallSyndication)
            {
                attributes = (JArray) DataAttributesEngine.GetAttribute ("application", applicationNumber, "companyCategoryReport").Result;
            }
            if (attributes == null || attributes.Count <= 0)
            {
                RuleResult result = null;
                FaultRetry.RunWithAlwaysRetry (() =>
                {
                    var companyDirectors = CompanyService.GetCompanyWithDirectors ("application", applicationNumber, cinNumber).Result;
                    result = CompanyService.GetCompanyCategory ("application", applicationNumber, companyDirectors, cinNumber).Result;
                });
                if (result != null)
                    referenceNumber = result.ReferenceNumber;
            }
            Logger.Info ($"[OrchestrationService/RunInitialOffer] CallCompanyDbSyndication method execution finished for [{applicationNumber}] Cin :[{cinNumber}]");
            return referenceNumber;
        }

        private IGetApplicationScoreResponse CallLenddoSyndication (string applicationNumber)
        {
            string referenceNumber = string.Empty;
            JArray attributes = null;

            attributes = (JArray) DataAttributesEngine.GetAttribute ("application", applicationNumber, "clientScoreCard").Result;
            IGetApplicationScoreResponse result = null;
            if (attributes == null || attributes.Count <= 0)
            {
                //  FaultRetry.RunWithAlwaysRetry (() => {
                result = LenddoService.GetClientScore ("application", applicationNumber, applicationNumber).Result;
                // });
            }
            return result;
        }

        private void CallViewDnsSyndication (string applicationNumber, string emailAddress, string companyName, string cinNumber, bool isReCallSyndication)
        {
            Logger.Info ($"[OrchestrationService/RunInitialOffer] CallViewDnsSyndication method execution started for [{applicationNumber}] email :[{emailAddress}] companyName:[{companyName}]  cinNumber:[{cinNumber}] ");
            string referenceNumber = string.Empty;
            Dictionary<string, object> attributes = null;
            if (!isReCallSyndication)
            {

            }
            if (attributes == null || attributes.Count <= 0)
            {
                // FaultRetry.RunWithAlwaysRetry (() => {
                ViewDnsService.VerifyEmployment ("application", applicationNumber, emailAddress, companyName, cinNumber).Wait ();
                // });
            }
            Logger.Info ($"[OrchestrationService/RunInitialOffer] CallViewDnsSyndication method execution finished for [{applicationNumber}] email :[{emailAddress}] companyName:[{companyName}]  cinNumber:[{cinNumber}] ");
        }

        private void CallEmailVerificationSyndication (string applicationNumber, string emailAddress, string name)
        {
            Logger.Info ($"[OrchestrationService/RunInitialOffer] CallEmailVerificationSyndication method execution started for [{applicationNumber}] email :[{emailAddress}] name:[{name}] ");
            TextInfo textInfo = new CultureInfo ("en-US", false).TextInfo;
            EmailVerificationRequest request = new EmailVerificationRequest
            {
                Email = emailAddress,
                Source = "borrower",
                Name = textInfo.ToTitleCase (name ?? string.Empty),
                Data = null
            };
            //FaultRetry.RunWithAlwaysRetry (() => {
            EmailVerificationService.InitiateEmailVerification ("application", applicationNumber, request).Wait ();
            //  });
            Logger.Info ($"[OrchestrationService/RunInitialOffer] CallEmailVerificationSyndication method execution finished for [{applicationNumber}] email :[{emailAddress}] name:[{name}] ");
        }
        #region Private Method

        private IProductRuleResult VerifyRule (string applicationNumber, string productId, string ruleType, string group,
            string rule)
        {
            Logger.Info ($"[OrchestrationService] VerifyRule method execution started for [{applicationNumber}] rule : [{rule}]");
            IProductRuleResult ruleExecutionResult = null;
            var count = 1;
            const int maxAttempt = 3;

            FaultRetry.RunWithAlwaysRetry (() =>
            {
                var result = ProductRuleService.RunRule ("application", applicationNumber, productId, rule).Result;

                if (result == null)
                    throw new OfferGenerationFailedException ("Unable to execute rule : " + rule);

                ruleExecutionResult = result;
                Logger.Info ($"[OrchestrationService] VerifyRule method execution intermediate result for [{applicationNumber}] rule :  [{rule}] result : [{  (ruleExecutionResult != null ? JsonConvert.SerializeObject(ruleExecutionResult) : null)}]");
                if (ruleExecutionResult == null || (ruleExecutionResult.Result == Result.Failed || ruleExecutionResult.Result == Result.UnDefined))
                {
                    if (count < maxAttempt)
                    {
                        count++;
                        if (ruleExecutionResult != null && (ruleExecutionResult.Result == Result.Failed || ruleExecutionResult.Result == Result.UnDefined))
                        {
                            if (ruleExecutionResult.ExceptionDetail != null && ruleExecutionResult.ExceptionDetail.Count > 0)
                            {
                                throw new OfferGenerationFailedException ("Unable to generate offer");
                            }
                            else if (ruleExecutionResult.ResultDetail != null && ruleExecutionResult.ResultDetail.Count > 0)
                            {
                                throw new Exception (ruleExecutionResult.ExceptionDetail.FirstOrDefault ());
                            }
                        }
                        throw new OfferGenerationFailedException ("Unable to execute rule");
                    }
                }
            });

            if (ruleExecutionResult == null)
                throw new OfferGenerationFailedException ("Unable to execute rule");

            if (ruleExecutionResult.Result == Result.Failed || ruleExecutionResult.Result == Result.UnDefined)
            {
                if (ruleExecutionResult.ExceptionDetail != null && ruleExecutionResult.ExceptionDetail.Count > 0)
                {
                    throw new OfferGenerationFailedException ("Unable to generate offer");
                }
                else if (ruleExecutionResult.ResultDetail != null && ruleExecutionResult.ResultDetail.Count > 0)
                {
                    throw new Exception (ruleExecutionResult.ExceptionDetail.FirstOrDefault ());
                }
            }
            Logger.Info ($"[OrchestrationService] VerifyRule method execution finished for [{applicationNumber}] result : [{  (ruleExecutionResult != null ? JsonConvert.SerializeObject(ruleExecutionResult) : null)}]");
            return ruleExecutionResult;
        }

        private async Task<IApplicantInformation> GetApplicationInformation (string applicationNumber)
        {
            JArray attributes = (JArray) await DataAttributesEngine.GetAttribute ("application", applicationNumber, "application");

            if (attributes == null || attributes.Count <= 0)
                throw new OfferGenerationFailedException ("No data attributes found for application source.");

            var applicantInformation = JObject.FromObject (attributes.FirstOrDefault ()).ToObject<ApplicantInformation> ();
            return applicantInformation;
        }

        private static string GetFileTypeValue (dynamic data)
        {
            Func<dynamic> resultProperty = () => data.fileType;

            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }
        private static bool HasProperty<T> (Func<T> property)
        {
            try
            {
                property ();
                return true;
            }
            catch (RuntimeBinderException ex)
            {
                return false;
            }
        }

        private static T GetValue<T> (Func<T> property)
        {
            return property ();
        }

        public Task<IInitialOfferDefination> RunInitialOffer (string applicationNumber, bool isReCompute)
        {
            throw new NotImplementedException ();
        }

        public Task<IFinalOfferDefination> RunFinalOffer (string applicationNumber, bool isReCompute)
        {
            throw new NotImplementedException ();
        }
        #endregion
    }
}