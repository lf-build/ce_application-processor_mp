﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.ApplicationProcessor
{
    public class CompositeRecoverabilityPolicy : IRecoverabilityPolicy
    {
        private readonly IEnumerable<IRecoverabilityPolicy> _policies;
        public CompositeRecoverabilityPolicy(IEnumerable<IRecoverabilityPolicy> policies)
        {
            if (policies == null)
            {
                throw new ArgumentNullException(nameof(policies));
            }
            _policies = policies;
        }
        public bool CanBeRecoveredFrom(Exception ex)
        {
            return _policies.Any(p => p.CanBeRecoveredFrom(ex));
        }
    }
}