﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Consent;
using LendFoundry.DataAttributes;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Services;
using LendFoundry.StatusManagement;
using LendFoundry.VerificationEngine;
// using CreditExchange.Syndication.EmailHunter;
using System.Globalization;
using LendFoundry.Application.Document;
using LendFoundry.DocumentGenerator;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.ProductRule;
using LendFoundry.TemplateManager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CreditExchange.ApplicationProcessor
{
    public class OfferEngineService : IOfferEngineService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OfferEngineService"/> class.
        /// </summary>
        /// <param name="eventHub">
        /// The event hub.
        /// </param>
        /// <param name="orchestrationService">
        /// The orchestration service.
        /// </param>
        /// <param name="offerEngineRepository">
        /// The offer engine repository.
        /// </param>
        /// <param name="finalOfferEngineRepository">
        /// The final offer engine repository.
        /// </param>
        /// <param name="entityStatusService">
        /// The entity status service.
        /// </param>
        /// <param name="dataAttributeEngine">
        /// The data attribute engine.
        /// </param>
        /// <param name="verificationEngineService">
        /// The verification engine service.
        /// </param>
        /// <param name="applicationProcessorConfiguration">
        /// The application processor configuration.
        /// </param>
        /// <param name="consentService">
        /// The consent service.
        /// </param>
        /// <param name="applicationService">
        /// The application service.
        /// </param>
        /// <param name="emailHunterService">
        /// The email hunter service.
        /// </param>
        /// <param name="decisionEngineService">
        /// The decision engine service.
        /// </param>
        /// <param name="lookupService">
        /// The lookup service.
        /// </param>
        /// <param name="tenantTime">
        /// The tenant time.
        /// </param>
        /// <param name="logger">
        /// The logger.
        /// </param>
        public OfferEngineService (IEventHubClient eventHub,
            IOrchestrationService orchestrationService, IInitialOfferEngineRepository offerEngineRepository,
            IFinalOfferEngineRepository finalOfferEngineRepository, IEntityStatusService entityStatusService,
            IDataAttributesEngine dataAttributeEngine, IVerificationEngineService verificationEngineService, ApplicationProcessorConfiguration applicationProcessorConfiguration, IConsentService consentService,
            Application.IApplicationService applicationService, IDecisionEngineService decisionEngineService,
            ILookupService lookupService, ITenantTime tenantTime, ILogger logger, IDocumentGeneratorService documentGeneratorService, IApplicationDocumentService applicationDocumentService, IProductRuleService productRuleService, Application.IApplicationService externalApplicationService)
        {
            EventHub = eventHub;
            OrchestrationService = orchestrationService;
            InitialOfferEngineRepository = offerEngineRepository;
            FinalOfferEngineRepository = finalOfferEngineRepository;
            EntityStatusService = entityStatusService;
            DataAttributesEngine = dataAttributeEngine;
            VerificationEngineService = verificationEngineService;
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
            ConsentService = consentService;
            // ExternalApplicationService = applicationService;
            //EmailHunterService = emailHunterService;
            DecisionEngineService = decisionEngineService;
            LookupService = lookupService;
            TenantTime = tenantTime;
            Logger = logger;
            DocumentGeneratorService = documentGeneratorService;
            ApplicationDocumentService = applicationDocumentService;
            ProductRuleService = productRuleService;
            ExternalApplicationService = externalApplicationService;
        }
        private ApplicationProcessorConfiguration ApplicationProcessorConfiguration { get; }
        private IInitialOfferEngineRepository InitialOfferEngineRepository { get; }
        private IOrchestrationService OrchestrationService { get; }
        private IEventHubClient EventHub { get; }
        private IFinalOfferEngineRepository FinalOfferEngineRepository { get; }
        private IEntityStatusService EntityStatusService { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private IVerificationEngineService VerificationEngineService { get; }
        private IConsentService ConsentService { get; }
        private ILookupService LookupService { get; }
        private IApplicationService ApplicationService { get; set; }
        //private IEmailHunterService EmailHunterService { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        // private Application.IApplicationService ExternalApplicationService { get; }
        private IProductRuleService ProductRuleService { get; }
        private Application.IApplicationService ExternalApplicationService { get; }
        private ITenantTime TenantTime { get; }

        private ILogger Logger { get; }

        private IDocumentGeneratorService DocumentGeneratorService { get; }

        private IApplicationDocumentService ApplicationDocumentService { get; }
        public async Task<IInitialOfferDefination> InitiateInitialOffer (string applicationNumber, bool isReCallSyndication)
        {
            Logger.Info ($"[OfferEngineService] InitiateInitialOffer method  execution started for [{applicationNumber}]");
            if (string.IsNullOrEmpty (applicationNumber))
            {
                throw new ArgumentException ("Application number is mandatory");
            }
            Logger.Info ($"[OfferEngineService/InitiateInitialOffer] GetStatusByEntity method execution started for [{applicationNumber}] ");
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            var status = await EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId);
            Logger.Info ($"[OfferEngineService/InitiateInitialOffer] GetStatusByEntity method execution finished for [{applicationNumber}] ");
            if (ApplicationProcessorConfiguration != null && status != null && ApplicationProcessorConfiguration.Statuses["ApplicationSubmitted"] != status.Code)
            {
                throw new InvalidOperationException ($"Can not initiate offer when status is {status.Code}");
            }
            var eventNameInitial = "InitialofferInitiated";
            Logger.Info ($"[OfferEngineService/InitiateInitialOffer] Published event InitialofferInitiated for [{applicationNumber}] ");
            await EventHub.Publish (eventNameInitial, new { ApplicationNumber = applicationNumber, IsFullRefresh = isReCallSyndication });
            await EventHub.Publish (eventNameInitial + "_" + applicationNumber, new { ApplicationNumber = applicationNumber, IsFullRefresh = isReCallSyndication });

            var initialOffer = await OrchestrationService.RunInitialOffer (applicationNumber, isReCallSyndication, activeWorkflow.ProductId);
            activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);

            var eventName = "InitialofferCompleted";

            // ToDo: Rules will return the reason code which will match with status management
            switch (initialOffer.Status)
            {
                case ScoreCardApplicationStatus.Completed:
                    Logger.Info ($"[OfferEngineService/InitiateInitialOffer] ChangeStatus execution started for [{applicationNumber}] newStatus :[{ApplicationProcessorConfiguration.Statuses["InitialOfferGiven"]}] ");
                    await EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["InitialOfferGiven"], new RequestModel ());
                    Logger.Info ($"[OfferEngineService/InitiateInitialOffer] ChangeStatus execution Finished for [{applicationNumber}] newStatus :[{ApplicationProcessorConfiguration.Statuses["InitialOfferGiven"]}] ");
                    break;
                case ScoreCardApplicationStatus.Rejected:
                    Logger.Info ($"[OfferEngineService/InitiateInitialOffer] ChangeStatus execution started for [{applicationNumber}] newStatus :[{ApplicationProcessorConfiguration.Statuses["Rejected"]}] Reasons :[{initialOffer.Reasons}] ");
                    await EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Rejected"], new RequestModel () { reasons = initialOffer.Reasons });
                    await ApplicationRejectedEventPublish (applicationNumber, status, initialOffer.Reasons);
                    Logger.Info ($"[OfferEngineService/InitiateInitialOffer] ChangeStatus execution Finished for [{applicationNumber}]  newStatus :[{ApplicationProcessorConfiguration.Statuses["Rejected"]}] Reasons :[{initialOffer.Reasons}] ");
                    break;
                case ScoreCardApplicationStatus.Failed:
                    await EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["ApplicationSubmitted"], new RequestModel ());
                    eventName = "InitialofferFailed";
                    break;
            }
            Logger.Info ($"[OfferEngineService/InitiateInitialOffer] Published event InitialofferCompleted for [{applicationNumber}] ");
            await EventHub.Publish (eventName, new { InitialOffer = initialOffer });
            await EventHub.Publish ("InitialOfferProcessed_" + applicationNumber, new { InitialOffer = initialOffer });
            Logger.Info ($"[OfferEngineService] InitiateInitialOffer method  execution finished for [{applicationNumber}]");
            return initialOffer;
        }

        /// <summary>
        /// RepullCibil
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<bool> RepullCibil (string applicationNumber, string productId)
        {
            Logger.Debug ($"[OfferEngineService] RepullCibil method  execution started for [{applicationNumber}] and [{productId}]");
            var flag = await OrchestrationService.RepullCibil (applicationNumber, productId);
            if (flag)
            {
                await EventHub.Publish ("CibilReportRepulled", new { ApplicationNumber = applicationNumber, ProductId = productId });
            }
            return flag;
        }

        public async Task<IInitialOfferDefination> GetInitialOffer (string entityId)
        {
            var initialOffers = await InitialOfferEngineRepository.GetInitialOffers ("application", entityId);
            if (initialOffers == null)
            {
                return null;
            }
            return initialOffers;
        }

        public async Task<IFinalOfferDefination> InitiateFinalOffer (string entityId, bool isReCallSyndication)
        {
            if (string.IsNullOrEmpty (entityId))
                throw new ArgumentException ("entity id is mandatory");
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", entityId);
            var status = await EntityStatusService.GetStatusByEntity ("application", entityId, activeWorkflow.StatusWorkFlowId);

            if (ApplicationProcessorConfiguration == null || (ApplicationProcessorConfiguration.Statuses["InitialOfferSelected"] != status.Code && ApplicationProcessorConfiguration.Statuses["ManualReview"] != status.Code))
            {
                throw new InvalidOperationException ($"Can not initiate final offer when status is {status.Code}");
            }
            var eventNameInitial = "FinalofferInitiated";
            await EventHub.Publish (eventNameInitial, new { ApplicationNumber = entityId, IsFullRefresh = isReCallSyndication });
            await EventHub.Publish (eventNameInitial + "_" + entityId, new { ApplicationNumber = entityId, IsFullRefresh = isReCallSyndication });

            var finalOffer = await OrchestrationService.RunFinalOffer (entityId, isReCallSyndication, activeWorkflow.ProductId, activeWorkflow.StatusWorkFlowId).ConfigureAwait (false);

            try
            {
                if (finalOffer.Status == ScoreCardApplicationStatus.Completed)
                    await EntityStatusService.ChangeStatus ("application", entityId, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Finaloffermade"], new RequestModel ());
                else if (finalOffer.Status == ScoreCardApplicationStatus.Rejected)
                {
                    await EntityStatusService.ChangeStatus ("application", entityId, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Rejected"], new RequestModel () { reasons = finalOffer.Reasons }); // ToDo : Rules will return the reason code which will match with status management
                    await ApplicationRejectedEventPublish (entityId, status, finalOffer.Reasons);
                }
                else if (finalOffer.Status == ScoreCardApplicationStatus.ManualReview && ApplicationProcessorConfiguration.Statuses["ManualReview"] != status.Code)
                    await EntityStatusService.ChangeStatus ("application", entityId, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["ManualReview"], new RequestModel () { reasons = finalOffer.Reasons }); // ToDo: Rules will return the reason code which will match with status management
            }
            catch (Exception ex)
            {
                Logger.Info (ex.Message);
            }

            var eventName = string.Empty;
            switch (finalOffer.Status)
            {
                case ScoreCardApplicationStatus.Completed:
                case ScoreCardApplicationStatus.Rejected:
                    eventName = "FinalofferCompleted";
                    await EventHub.Publish (eventName, new { FinalOffer = finalOffer }).ConfigureAwait (false);
                    if (finalOffer.Status == ScoreCardApplicationStatus.Completed)
                    {
                        eventName = "FinalOfferSucceed";
                        await EventHub.Publish (eventName, new { FinalOffer = finalOffer }).ConfigureAwait (false);
                    }
                    break;
                case ScoreCardApplicationStatus.Failed:
                    eventName = "FinalofferFailed";
                    await EventHub.Publish (eventName, new { FinalOffer = finalOffer }).ConfigureAwait (false);
                    break;
                case ScoreCardApplicationStatus.ManualReview:
                    await EventHub.Publish (
                        "ManualReviewNeeded",
                        new
                        {
                            ApplicationNumber = finalOffer.EntityId,
                                IsManualReviewNeeded = finalOffer.FinalOffers[0].ManualReview
                        }).ConfigureAwait (false);
                    break;
            }

            await EventHub.Publish ("FinalOfferProcessed", new { FinalOffer = finalOffer }).ConfigureAwait (false);

            return finalOffer;
        }

        public async Task<IFinalOfferDefination> GetFinalOffer (string applicationNumber, string filter = null)
        {
            var finalOffers = await FinalOfferEngineRepository.GetFinalOffers ("application", applicationNumber, filter).ConfigureAwait (false);
            if (finalOffers == null)
            {
                return null;
            }

            return finalOffers;
        }

        //TODO: Change code to have two method one for accept which have applicationNumber and OfferId, and one for rject which have applicationNumber and OfferId and reason

        //TODO: Change code to have two method one for accept which have applicationNumber and OfferId, and one for rject which have applicationNumber and OfferId and reason
        public async Task RejectInitialOffer (string applicationNumber, List<string> reasons)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentException ("Application number is mandatory");
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            await EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["NotInterested"], new RequestModel () { reasons = reasons.ToList () });

            //await OfferEngineRepository.SetSelectedInitialOffer("application", applicationNumber, string.Empty, false);

            await EventHub.Publish (new InitialOfferSelected { EntityType = "application", EntityId = applicationNumber, IsAccepted = false });
        }
        public async Task SelectInitialOffer (string applicationNumber, string offerId)
        {

            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentException ("Application number is mandatory");

            if (string.IsNullOrEmpty (offerId))
                throw new ArgumentException ("Offer id is mandatory");
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            var status = await EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId);
            if (ApplicationProcessorConfiguration.Statuses["InitialOfferGiven"] != status.Code)
            {
                throw new InvalidOperationException ($"Can not accept initial offer when status is {status.Code}");
            }

            var initialOffer = await InitialOfferEngineRepository.SetSelectedInitialOffer ("application", applicationNumber, offerId, TenantTime.Now);

            // Initiate manual verification of Employment at the time of initial offer selected

            //await EventHub.Publish (new InitialOfferSelected { EntityType = "application", EntityId = applicationNumber, SelectedInitialOffer = initialOffer, IsAccepted = true });

            /// Initiate manual verification of Employment at the time of initial offer selected
            //var newActiveWorkflow = await EntityStatusService.GetActiveStatusWorkFlow("application", applicationNumber);
            IVerificationFacts employmentVerification = null;
            try
            {
                employmentVerification = await VerificationEngineService.GetStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, "EmploymentVerification");
            }
            catch (Exception veException) { }
            if (employmentVerification == null || employmentVerification.VerificationStatus != VerificationStatus.Passed)
            {
                await VerificationEngineService.InitiateManualVerification ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, "EmploymentVerification", "EmploymentForm26ASManual");
            }
            var cibilData = await DataAttributesEngine.GetAttribute ("application", applicationNumber, "cibilReport");
            JToken bureauScore;
            JToken ecnnumber;
            //var cibilReport = JObject.FromObject (cibilData);
            var cibilReport = JObject.FromObject (JsonConvert.DeserializeObject<dynamic> (JsonConvert.SerializeObject (cibilData)) [0]);
            string cibilScore = string.Empty;
            string ecnNumber = string.Empty;
            if (cibilReport.TryGetValue ("bureauScore", out bureauScore))
            {
                cibilScore = Convert.ToString (bureauScore);
            }
            if (cibilReport.TryGetValue ("ecnnumber", out ecnnumber))
            {
                ecnNumber = Convert.ToString (ecnnumber);
            }
            await EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["InitialOfferSelected"], new RequestModel ());
            await EventHub.Publish (new InitialOfferSelected
            {
                EntityType = "application",
                    EntityId = applicationNumber,
                    SelectedInitialOffer = initialOffer,
                    IsAccepted = true,
                    CibilScore = cibilScore,
                    EcnNumber = ecnNumber
            });
        }

        public async Task RejectFinalOffer (string applicationNumber, List<string> reasons)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentException ("Application number is mandatory");
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            await EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["NotInterested"], new RequestModel () { reasons = reasons.ToList () });

            //await OfferEngineRepository.SetSelectedInitialOffer("application", applicationNumber, string.Empty, false);
            //TODO: Event should have object of offer selected
            await EventHub.Publish (new FinalOfferSelected { EntityType = "application", EntityId = applicationNumber, IsAccepted = false });
        }

        public async Task SelectFinalOffer (string applicationNumber, string offerId)
        {

            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentException ("Application number is mandatory");
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            var status = await EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId);

            if (ApplicationProcessorConfiguration.Statuses["Finaloffermade"] != status.Code)
            {
                throw new InvalidOperationException ($"Can not accept final offer when status is {status.Code}");
            }

            var selectedFinalOffer = await FinalOfferEngineRepository.SetSelectedFinalOffer ("application", applicationNumber, offerId, TenantTime.Now);

            //else if (viewDnsVerification.Attempts != null && viewDnsVerification.Attempts.Count > 0)
            //{
            //    var isVerified = viewDnsVerification.Attempts.OrderByDescending(x => x.DateAttempt).First().Result;
            //    if (isVerified != VerificationResult.Passed)
            //    {
            //        await VerificationEngineService.InitiateManualVerification("application", applicationNumber, "EmploymentVerification");
            //    }
            //}
            await VerificationEngineService.InitiateManualVerification ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, "KYC-PAN", "KYCPan");

            await VerificationEngineService.InitiateManualVerification ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, "KYC-Address", "KYCAddress");

            // Signed - Document Initiated 
            await VerificationEngineService.InitiateManualVerification ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, "SignedDocumentVerification", "SignedDocuments");
            //var loanAgreement = await SignLoanAggrement(applicationNumber);
            //var loanApplicationForm = await LoanApplicationForm(applicationNumber, loanAgreement);
            var applicationAttributes = await DataAttributesEngine.GetAllAttributes ("application", applicationNumber);
            await EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["FinalofferAccepted"], new RequestModel ());
            await EventHub.Publish (new FinalOfferSelected { EntityType = "application", EntityId = applicationNumber, SelectedFinalOffer = selectedFinalOffer, IsAccepted = true, Applicaiton = applicationAttributes });

            // await GenerateDrfandDpn(applicationNumber, selectedFinalOffer);
        }

        //TODO: Move it to application
        public async Task UpdateCashFlow (string entityId, object value)
        {
            await DataAttributesEngine.SetAttribute ("application", entityId, "perfiosReport", value);
        }
        public async Task AddInitialOffer (string applicationNumber, IInitialOffer initialOffer)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentException ("Application number is mandatory");

            if (initialOffer == null)
                throw new ArgumentException ("offer is mandatory");

            initialOffer.IsManualOffer = true;
            initialOffer.OfferId = Guid.NewGuid ().ToString ("N");

            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            var status = await EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId);

            if (initialOffer.IsSelected && ApplicationProcessorConfiguration.Statuses["InitialOfferGiven"] != status.Code)
            {
                throw new InvalidOperationException ($"Can not accept initial offer when status is {status.Code}");
            }
            await InitialOfferEngineRepository.AddInitialOffer ("application", applicationNumber, initialOffer);
            if (initialOffer.IsSelected)
            {
                await SelectInitialOffer (applicationNumber, initialOffer.OfferId);
                //EntityStatusService.ChangeStatus("application", applicationNumber, ApplicationProcessorConfiguration.Statuses["InitialOfferSelected"]);
                //await EventHub.Publish(new InitialOfferSelected { EntityType = "application", EntityId = applicationNumber, SelectedInitialOffer = initialOffer, IsAccepted = true });
            }
            await EventHub.Publish (new ManualInitialOfferAdded { EntityType = "application", EntityId = applicationNumber, ManualOffer = initialOffer });
        }

        public async Task AddFinalOffer (string applicationNumber, IFinalOffer finalOffer)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentException ("Application number is mandatory");

            if (finalOffer == null)
                throw new ArgumentException ("offer is mandatory");

            if (string.IsNullOrEmpty (finalOffer.Loantenure))
                throw new ArgumentException ("Loan tenure is mandatory");

            if (finalOffer.FinalOfferAmount < ApplicationProcessorConfiguration.MinFinalOfferAmount || finalOffer.FinalOfferAmount > ApplicationProcessorConfiguration.MaxFinalOfferAmount)
                throw new ArgumentException ("Loan amount should be greater than " + ApplicationProcessorConfiguration.MinFinalOfferAmount + " and less than " + ApplicationProcessorConfiguration.MaxFinalOfferAmount);

            if (finalOffer.InterestRate <= 0)
                throw new ArgumentException ("Interest rate should be greater than zero");

            if (finalOffer.ProcessingFee <= 0)
                throw new ArgumentException ("Processing Fees should be greater than zero");

            finalOffer.IsManualOffer = true;
            finalOffer.OfferId = Guid.NewGuid ().ToString ("N");

            await FinalOfferEngineRepository.AddFinalOffer ("application", applicationNumber, finalOffer);
            await EventHub.Publish (new ManualFinalOfferAdded { EntityType = "application", EntityId = applicationNumber, ManualOffer = finalOffer });
        }
        private async Task<ApplicationAttribute> SignLoanAggrement (string applicationNumber)
        {
            var attributes = await DataAttributesEngine.GetAttribute ("application", applicationNumber, "application");
            var finalOffer = await FinalOfferEngineRepository.GetFinalOffers ("application", applicationNumber);
            var dateOfEmi = TenantTime.Now;
            if (TenantTime.Now.Day < 20)
            {
                dateOfEmi = TenantTime.Now.AddMonths (1);
                dateOfEmi = TenantTime.Create (dateOfEmi.Year, dateOfEmi.Month, 5);
            }
            else
            {
                dateOfEmi = TenantTime.Create (dateOfEmi.Year, dateOfEmi.Month, 5);
            }
            var applicationAttribute = JsonConvert.DeserializeObject<List<ApplicationAttribute>> (attributes.ToString ());

            var applicationAttributeResult = GetModifiedData (applicationAttribute.FirstOrDefault ());

            var finalOfferSelected = finalOffer.FinalOffers != null ? finalOffer.FinalOffers.Where (s => s.IsSelected).FirstOrDefault () : null;
            var finalOfferAmount = finalOfferSelected != null ? finalOfferSelected.FinalOfferAmount.ToString ("#,##0") : string.Empty;
            var emi = finalOfferSelected != null ? finalOfferSelected.Emi.ToString ("#,##0") : string.Empty;

            var body = new { IPAddress = "10.1.1.99", SignedBy = "1", dataAttribute = applicationAttribute, offerSelected = finalOfferSelected, finalOfferAmount = finalOfferAmount, emi = emi, LoanDate = TenantTime.Now.ToString ("dd-MM-yyyy"), DateofEMI = dateOfEmi.ToString ("dd-MM-yyyy"), AdvanceEMI = 0, AdvanceEMIAmount = 0 };

            await ConsentService.Sign ("application", applicationNumber, "loanAgreement", body);

            return applicationAttributeResult;
        }

        private ApplicationAttribute GetModifiedData (ApplicationAttribute applicationAttribute)
        {
            var textInfo = new CultureInfo ("en-US", false).TextInfo;
            applicationAttribute.firstName = textInfo.ToTitleCase (applicationAttribute.firstName ?? string.Empty);
            applicationAttribute.middleName = textInfo.ToTitleCase (applicationAttribute.middleName ?? string.Empty);
            applicationAttribute.lastName = textInfo.ToTitleCase (applicationAttribute.lastName ?? string.Empty);
            applicationAttribute.pan = textInfo.ToTitleCase (applicationAttribute.pan ?? string.Empty);
            applicationAttribute.designation = textInfo.ToTitleCase (applicationAttribute.designation ?? string.Empty);
            if (applicationAttribute.addresses != null && applicationAttribute.addresses.FirstOrDefault () != null)
            {
                var addressDetail = applicationAttribute.addresses.FirstOrDefault ();
                applicationAttribute.addresses.FirstOrDefault ().AddressLine1 = textInfo.ToTitleCase (addressDetail.AddressLine1 ?? string.Empty);
                applicationAttribute.addresses.FirstOrDefault ().AddressLine2 = textInfo.ToTitleCase (addressDetail.AddressLine2 ?? string.Empty);
                applicationAttribute.addresses.FirstOrDefault ().LandMark = textInfo.ToTitleCase (addressDetail.LandMark ?? string.Empty);
                applicationAttribute.addresses.FirstOrDefault ().City = textInfo.ToTitleCase (addressDetail.City ?? string.Empty);
                applicationAttribute.addresses.FirstOrDefault ().State = textInfo.ToTitleCase (addressDetail.State ?? string.Empty);
                applicationAttribute.addresses.FirstOrDefault ().Country = textInfo.ToTitleCase (addressDetail.Country ?? string.Empty);
            }
            if (applicationAttribute.currentAddress != null)
            {
                applicationAttribute.currentAddress.AddressLine1 = textInfo.ToTitleCase (applicationAttribute.currentAddress.AddressLine1 ?? string.Empty);
                applicationAttribute.currentAddress.AddressLine2 = textInfo.ToTitleCase (applicationAttribute.currentAddress.AddressLine2 ?? string.Empty);
                applicationAttribute.currentAddress.LandMark = textInfo.ToTitleCase (Convert.ToString (applicationAttribute.currentAddress.LandMark) ?? string.Empty);
                applicationAttribute.currentAddress.City = textInfo.ToTitleCase (applicationAttribute.currentAddress.City ?? string.Empty);
                applicationAttribute.currentAddress.State = textInfo.ToTitleCase (applicationAttribute.currentAddress.State ?? string.Empty);
                applicationAttribute.currentAddress.Country = textInfo.ToTitleCase (applicationAttribute.currentAddress.Country ?? string.Empty);
            }
            if (applicationAttribute.permanentAddress != null)
            {
                applicationAttribute.permanentAddress.AddressLine1 = textInfo.ToTitleCase (applicationAttribute.permanentAddress.AddressLine1 ?? string.Empty);
                applicationAttribute.permanentAddress.AddressLine2 = textInfo.ToTitleCase (applicationAttribute.permanentAddress.AddressLine2 ?? string.Empty);
                applicationAttribute.permanentAddress.LandMark = textInfo.ToTitleCase (Convert.ToString (applicationAttribute.permanentAddress.LandMark) ?? string.Empty);
                applicationAttribute.permanentAddress.City = textInfo.ToTitleCase (applicationAttribute.permanentAddress.City ?? string.Empty);
                applicationAttribute.permanentAddress.State = textInfo.ToTitleCase (applicationAttribute.permanentAddress.State ?? string.Empty);
                applicationAttribute.permanentAddress.Country = textInfo.ToTitleCase (applicationAttribute.permanentAddress.Country ?? string.Empty);
            }

            if (applicationAttribute.employmentAddress != null)
            {
                applicationAttribute.employmentAddress.FirstOrDefault ().AddressLine1 = textInfo.ToTitleCase (applicationAttribute.employmentAddress.FirstOrDefault ()?.AddressLine1 ?? string.Empty);
                applicationAttribute.employmentAddress.FirstOrDefault ().AddressLine2 = textInfo.ToTitleCase (applicationAttribute.employmentAddress.FirstOrDefault ()?.AddressLine2 ?? string.Empty);
                applicationAttribute.employmentAddress.FirstOrDefault ().LandMark = textInfo.ToTitleCase (Convert.ToString (applicationAttribute.employmentAddress.FirstOrDefault ()?.LandMark) ?? string.Empty);
                applicationAttribute.employmentAddress.FirstOrDefault ().City = textInfo.ToTitleCase (applicationAttribute.employmentAddress.FirstOrDefault ()?.City ?? string.Empty);
                applicationAttribute.employmentAddress.FirstOrDefault ().State = textInfo.ToTitleCase (applicationAttribute.employmentAddress.FirstOrDefault ()?.State ?? string.Empty);
                applicationAttribute.employmentAddress.FirstOrDefault ().Country = textInfo.ToTitleCase (applicationAttribute.employmentAddress.FirstOrDefault ()?.Country ?? string.Empty);
                applicationAttribute.employmentAddress.FirstOrDefault ().PinCode = textInfo.ToTitleCase (applicationAttribute.employmentAddress.FirstOrDefault ()?.PinCode ?? string.Empty);
            }
            if (applicationAttribute.educationalQualification != null)
            {
                var levelOfEducation = LookupService.GetLookupEntry ("application-levelOfEducation", Convert.ToString (applicationAttribute.educationalQualification));
                applicationAttribute.educationalQualification = levelOfEducation.Select (l => l.Value).FirstOrDefault ();
            }

            var lookupValue = LookupService.GetLookupEntry ("application-purposeOfLoan", applicationAttribute.purposeOfLoan);
            applicationAttribute.purposeOfLoan = lookupValue.Select (l => l.Value).FirstOrDefault ();

            var date = applicationAttribute.dateOfBirth.Split ('-');
            applicationAttribute.dateOfBirth = (new DateTime (Convert.ToInt32 (date[0]), Convert.ToInt32 (date[1]), Convert.ToInt32 (date[2]))).ToString ("dd-MM-yyyy");
            applicationAttribute.customIncome = Convert.ToDecimal (applicationAttribute.income).ToString ("#,##0");

            var applicationDate = Convert.ToString (applicationAttribute.applicationDate.Time);
            date = applicationDate.Split ('-');
            var generatedApplicationDate = (new DateTime (Convert.ToInt32 (date[0]), Convert.ToInt32 (date[1]), Convert.ToInt32 (date[2]))).ToString ("dd-MM-yyyy");
            applicationAttribute.customApplicationDate = generatedApplicationDate;

            return applicationAttribute;
        }

        private async Task<IConsent> LoanApplicationForm (string applicationNumber, ApplicationAttribute applicationAttribute)
        {
            List<string> dataattributes = new List<string> { "ancillaryData", "companyCategoryReport" };
            var attributes = await DataAttributesEngine.GetAttribute ("application", applicationNumber, dataattributes);
            var finalOffer = await FinalOfferEngineRepository.GetFinalOffers ("application", applicationNumber);
            object val = null;
            List<AncillaryData> ancillaryData = new List<AncillaryData> ();
            List<CompanyCategory> companyCategory = new List<CompanyCategory> ();
            if (attributes.TryGetValue ("ancillaryData", out val))
            {
                ancillaryData = JsonConvert.DeserializeObject<List<AncillaryData>> (attributes["ancillaryData"].ToString ());
            }
            if (attributes.TryGetValue ("companyCategoryReport", out val))
            {
                companyCategory = JsonConvert.DeserializeObject<List<CompanyCategory>> (attributes["companyCategoryReport"].ToString ());
            }
            var finalOfferSelected = finalOffer.FinalOffers != null ? finalOffer.FinalOffers.Where (s => s.IsSelected).FirstOrDefault () : null;
            var finalOfferAmount = finalOfferSelected != null ? finalOfferSelected.FinalOfferAmount.ToString ("#,##0") : string.Empty;

            var body = new { IPAddress = "10.1.1.99", SignedBy = "1", dataAttribute = applicationAttribute, offerSelected = finalOfferSelected, finalOfferAmount = finalOfferAmount, ancillaryData = ancillaryData.FirstOrDefault (), applicationNumber = applicationNumber, companyCategory = companyCategory.FirstOrDefault () };

            return await ConsentService.Sign ("application", applicationNumber, "LoanApplicationForm", body);
        }
        public async Task UpdateFinalOfferIntermediateData (string applicationnumber, string grade, string score)
        {
            var scoreCardDetail = await DataAttributesEngine.GetAttribute ("application", applicationnumber, "FinalOfferCalculation");
            try
            {
                var scoreCardDetailData = JsonConvert.DeserializeObject<List<object>> (scoreCardDetail.ToString ());

                var intermidiateResult =
                    JObject.FromObject (scoreCardDetailData.FirstOrDefault ())
                    .ToObject<Dictionary<string, object>> ();
                var finalOffer = JArray.FromObject (intermidiateResult["offerData"]);

                foreach (var item in finalOffer)
                {
                    item["score"] = score;
                    item["grade"] = grade;
                }

                intermidiateResult["offerData"] = finalOffer;
                await DataAttributesEngine.SetAttribute ("application", applicationnumber, "FinalOfferCalculation", intermidiateResult);
            }
            catch (Exception ex)
            {
                Logger.Info (ex.Message);
            }
        }

        public async Task RepresentManualOffer (string applicationNumber, IFinalOffer finalOffer)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentException ("Application number is mandatory");

            if (finalOffer == null)
                throw new ArgumentException ("offer is mandatory");

            if (string.IsNullOrEmpty (finalOffer.Loantenure))
                throw new ArgumentException ("Loan tenure is mandatory");

            if (finalOffer.FinalOfferAmount < ApplicationProcessorConfiguration.MinFinalOfferAmount || finalOffer.FinalOfferAmount > ApplicationProcessorConfiguration.MaxFinalOfferAmount)
                throw new ArgumentException ("Loan amount should be greater than " + ApplicationProcessorConfiguration.MinFinalOfferAmount + " and less than " + ApplicationProcessorConfiguration.MaxFinalOfferAmount);

            if (finalOffer.InterestRate <= 0)
                throw new ArgumentException ("Interest rate should be greater than zero");

            if (finalOffer.ProcessingFee <= 0)
                throw new ArgumentException ("Processing Fees should be greater than zero");

            finalOffer.IsManualOffer = true;
            finalOffer.OfferId = Guid.NewGuid ().ToString ("N");

            await FinalOfferEngineRepository.AddFinalOffer ("application", applicationNumber, finalOffer);
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            if (activeWorkflow == null)
                throw new ArgumentException ($"No active workflow available for {applicationNumber}");
            var currentApplicationStatus = await EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId);
            if (currentApplicationStatus == null)
            {
                throw new NotFoundException ($"Current Status of Application Number {applicationNumber} can not be found");
            }

            var finalOfferDetails = await FinalOfferEngineRepository.GetFinalOffers ("application", applicationNumber);

            if (finalOfferDetails == null)
                throw new NotFoundException ($"There is no final offer generated for {applicationNumber}");

            if (finalOfferDetails.FinalOffers == null)
                throw new NotFoundException ($"Final offer not found for {applicationNumber}");

            finalOfferDetails.Status = ScoreCardApplicationStatus.Completed;
            finalOfferDetails.Reasons = null;
            foreach (var item in finalOfferDetails.FinalOffers)
            {
                item.ManualReview = false;
                if (finalOffer.OfferId != null)
                {
                    if (finalOffer.OfferId == item.OfferId)
                    {
                        item.IsPresented = true;
                    }
                }
            }
            FinalOfferEngineRepository.Update (finalOfferDetails);
            const string EventName = "FinalOfferRepresented";
            await EventHub.Publish (EventName, new { FinalOffer = finalOfferDetails });
        }

        private static JToken FindToken<T> (string key, string value)
        {

            var jObject = JObject.Parse (value);
            var jToken = jObject.SelectToken (key);
            if (jToken != null)
                return jToken;

            return null;
        }

        private async Task GenerateDrfandDpn (string applicationNumber, IFinalOffer selectedFinalOffer)
        {
            var applicationAttributes = await DataAttributesEngine.GetAllAttributes ("application", applicationNumber);

            var drfData = GetDrfData (new { ApplicationNumber = applicationNumber, DataAttributes = applicationAttributes, SelectedFinalOffer = selectedFinalOffer });
            var dpnData = GetDpnData (new { ApplicationNumber = applicationNumber, DataAttributes = applicationAttributes, SelectedFinalOffer = selectedFinalOffer });

            var drfDocument = await DocumentGeneratorService.Generate (ApplicationProcessorConfiguration.DrfConfiguration.TemplateName, ApplicationProcessorConfiguration.DrfConfiguration.TemplateVersion, Format.Html, new LendFoundry.DocumentGenerator.Document { Data = drfData, Format = DocumentFormat.Pdf, Name = ApplicationProcessorConfiguration.DrfConfiguration.DocumentName, Version = "1.0" });
            var dpnDocument = await DocumentGeneratorService.Generate (ApplicationProcessorConfiguration.DpnConfiguration.TemplateName, ApplicationProcessorConfiguration.DpnConfiguration.TemplateVersion, Format.Html, new LendFoundry.DocumentGenerator.Document { Data = dpnData, Format = DocumentFormat.Pdf, Name = ApplicationProcessorConfiguration.DpnConfiguration.DocumentName, Version = "1.0" });

            await ApplicationDocumentService.Add (entityType: "application", applicationNumber : applicationNumber, category : ApplicationProcessorConfiguration.DrfConfiguration.Category, fileBytes : drfDocument.Content, fileName : ApplicationProcessorConfiguration.DrfConfiguration.DocumentName, tags : ApplicationProcessorConfiguration.DrfConfiguration.Tags);
            await ApplicationDocumentService.Add (entityType: "application", applicationNumber : applicationNumber, category : ApplicationProcessorConfiguration.DpnConfiguration.Category, fileBytes : dpnDocument.Content, fileName : ApplicationProcessorConfiguration.DpnConfiguration.DocumentName, tags : ApplicationProcessorConfiguration.DpnConfiguration.Tags);
        }

        private object GetDrfData (object drfData)
        {
            try
            {
                var executionResult = DecisionEngineService.Execute<dynamic, dynamic> ("get_drf_data", new { input = drfData });
                return executionResult;
            }
            catch (Exception ex)
            {
                Logger.Error (ex.Message);
                return null;
            }
        }

        private object GetDpnData (object dpnData)
        {
            try
            {
                var executionResult = DecisionEngineService.Execute<dynamic, dynamic> ("get_dpn_data", new { input = dpnData });
                return executionResult;
            }
            catch (Exception ex)
            {
                Logger.Error (ex.Message);
                return null;
            }
        }

        private async Task ApplicationRejectedEventPublish (string entityId, IStatusResponse oldStatus, List<string> reasons)
        {
            await EventHub.Publish ("ApplicationRejected", new StatusChanged
            {
                EntityType = "application",
                    EntityId = entityId,
                    OldStatus = oldStatus?.Code,
                    OldStatusName = oldStatus?.Name,
                    NewStatus = "200.16",
                    NewStatusName = "Rejected",
                    Reason = reasons,
                    ActiveOn = new LendFoundry.Foundation.Date.TimeBucket (DateTime.Now)
            });
            await EventHub.Publish ("ApplicationRejectedForBank", new StatusChanged
            {
                EntityType = "application",
                    EntityId = entityId,
                    OldStatus = oldStatus?.Code,
                    OldStatusName = oldStatus?.Name,
                    NewStatus = "200.16",
                    NewStatusName = "Rejected",
                    Reason = reasons,
                    ActiveOn = new LendFoundry.Foundation.Date.TimeBucket (DateTime.Now)
            });
        }
    }
}