﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DataAttributes;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.StatusManagement;
using Newtonsoft.Json.Linq;

namespace CreditExchange.ApplicationProcessor {
    public class StatusManagementService : IStatusManagementService {
        public StatusManagementService (IEntityStatusService entityStatusService,
            ILookupService lookup,
            ApplicationProcessorConfiguration applicationProcessorConfiguration,
            IFinalOfferEngineRepository finalOfferEngineRepository,
            IEventHubClient eventHub,
            IDecisionEngineService decisionEngineService,
            Application.IApplicationService applicationService,
            IDataAttributesEngine dataAttributeEngine,
            ILogger logger) {
            if (entityStatusService == null)
                throw new ArgumentNullException (nameof (entityStatusService));

            EntityStatusService = entityStatusService;
            LookupService = lookup;
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
            FinalOfferEngineRepository = finalOfferEngineRepository;
            EventHub = eventHub;
            DecisionEngineService = decisionEngineService;
            ExternalApplicationService = applicationService;
            DataAttributesEngine = dataAttributeEngine;
            Logger = logger;
        }

        /// <summary>
        /// Gets the final offer engine repository.
        /// </summary>
        private IFinalOfferEngineRepository FinalOfferEngineRepository { get; }

        private IEntityStatusService EntityStatusService { get; }
        private ApplicationProcessorConfiguration ApplicationProcessorConfiguration { get; }
        private ILookupService LookupService { get; }
        private IEventHubClient EventHub { get; }
        private IDecisionEngineService DecisionEngineService { get; }

        private Application.IApplicationService ExternalApplicationService { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private ILogger Logger { get; }
        public async Task ChangeStatus (string entityId, string newStatus, List<string> reasons) {
            EnsureInputIsValid (entityId);
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", entityId);
            IStatusResponse oldStatus = EntityStatusService.GetStatusByEntity ("application", entityId,activeWorkflow.StatusWorkFlowId).Result;
            await Task.Run (() => EntityStatusService.ChangeStatus ("application", entityId, activeWorkflow.StatusWorkFlowId, newStatus, new RequestModel () { reasons = reasons })).ConfigureAwait (false);

            if (newStatus == ApplicationProcessorConfiguration.Statuses["Rejected"]) {
                await EventHub.Publish ("ApplicationRejected", new StatusChanged {
                    EntityType = "application",
                        EntityId = entityId,
                        OldStatus = oldStatus?.Code,
                        OldStatusName = oldStatus?.Name,
                        NewStatus = newStatus,
                        NewStatusName = "Rejected",
                        Reason = reasons,
                        ActiveOn = new LendFoundry.Foundation.Date.TimeBucket (DateTime.Now)
                });
                await EventHub.Publish ("ApplicationRejectedForBank", new StatusChanged {
                    EntityType = "application",
                        EntityId = entityId,
                        OldStatus = oldStatus?.Code,
                        OldStatusName = oldStatus?.Name,
                        NewStatus = newStatus,
                        NewStatusName = "Rejected",
                        Reason = reasons,
                        ActiveOn = new LendFoundry.Foundation.Date.TimeBucket (DateTime.Now)
                });
            }
        }

        public async Task RejectApplication (string entityId, List<string> reasons) {
            EnsureInputIsValid (entityId);
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", entityId);
            await Task.Run (() => EntityStatusService.ChangeStatus ("application", entityId, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Rejected"], new RequestModel () { reasons = reasons })).ConfigureAwait (false);
        }

        public async Task ApplicationStatusMove (string applicationNumber) {
            EnsureInputIsValid (applicationNumber);
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            var currentApplicationStatus = await EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId);
            if (currentApplicationStatus == null)
                throw new NotFoundException ($"Current Status of Application Number {applicationNumber} can not be found");

            if (currentApplicationStatus.Code == ApplicationProcessorConfiguration.Statuses["FinalofferAccepted"])
                await Task.Run (() => EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["CEApproved"], new RequestModel ()));
            else if (currentApplicationStatus.Code == ApplicationProcessorConfiguration.Statuses["BankCreditReview"])
                await Task.Run (() => EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["RBLFraud"], new RequestModel ()));
            else if (currentApplicationStatus.Code == ApplicationProcessorConfiguration.Statuses["RBLFraud"])
                await Task.Run (
                    () => EntityStatusService.ChangeStatus (
                        "application",
                        applicationNumber, activeWorkflow.StatusWorkFlowId,
                        ApplicationProcessorConfiguration.Statuses["Approved"], new RequestModel ()));
        }

        /// <summary>
        /// The final offer ready from manual review.
        /// </summary>
        /// <param name="applicationNumber">
        /// The application number.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotFoundException">
        /// </exception>
        /// <exception cref="InvalidOperationException">
        /// </exception>
        public async Task FinalOfferReadyFromManualReview (string applicationNumber, object offerId) {
            EnsureInputIsValid (applicationNumber);
            List<string> selectedofferid = null;
            if (offerId != null) {

                var offerid = ((IEnumerable) offerId).Cast<object> ()
                    .Select (x => x.ToString ())
                    .ToArray ();
                selectedofferid = new List<string> (offerid);
            }
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            var currentApplicationStatus = await EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId);
            if (currentApplicationStatus == null) {
                throw new NotFoundException ($"Current Status of Application Number {applicationNumber} can not be found");
            }

            if (currentApplicationStatus.Code != ApplicationProcessorConfiguration.Statuses["ManualReview"]) {
                throw new InvalidOperationException (
                    $"Current Status of Application Number {applicationNumber} is not ManualReview");
            }
            var finalOfferDetails = await FinalOfferEngineRepository.GetFinalOffers ("application", applicationNumber);

            if (finalOfferDetails == null)
                throw new NotFoundException ($"There is no final offer generated for {applicationNumber}");

            if (finalOfferDetails.FinalOffers == null)
                throw new NotFoundException ($"Final offer not found for {applicationNumber}");

            finalOfferDetails.Status = ScoreCardApplicationStatus.Completed;
            finalOfferDetails.Reasons = null;
            foreach (var item in finalOfferDetails.FinalOffers) {
                item.ManualReview = false;
                if (selectedofferid != null) {
                    if (selectedofferid.Count > 0) {
                        if (selectedofferid.Contains (item.OfferId)) {
                            item.IsPresented = true;
                        }
                    }
                }
            }

            FinalOfferEngineRepository.Update (finalOfferDetails);

            // TODO : future use
            //List<IScoreCardRuleResult> result = await ScoreCardService.GetScoreCardResultDetail("application", applicationNumber);

            //var finalOfferScoreRuleResult = result.Where(i => i.Name == "FinalOfferCalculation").OrderByDescending(i => i.ExecutedDate).FirstOrDefault();
            //if (finalOfferScoreRuleResult == null)
            //    throw new NotFoundException($"There is no final offer generated for {applicationNumber}");
            //if (finalOfferScoreRuleResult.IntermediateData != null)
            //{
            //    var intermidiateResult =
            //               JObject.FromObject(finalOfferScoreRuleResult.IntermediateData)
            //                   .ToObject<Dictionary<string, object>>();

            //    List<FinalOffer> dynObj = JsonConvert.DeserializeObject<List<FinalOffer>>(Convert.ToString(intermidiateResult["offerData"]));

            //    dynObj.ForEach(item => item.ManualReview = false);
            //    string jsonObject = Regex.Unescape(JsonConvert.SerializeObject(new { offerData = dynObj }, Newtonsoft.Json.Formatting.Indented));              
            //    dynamic intermediateData = JObject.Parse(jsonObject);                
            //    finalOfferScoreRuleResult.IntermediateData = intermediateData;
            //}

            //await ScoreCardService.UpdateScoreCardResult("application", applicationNumber, finalOfferScoreRuleResult);

            await Task.Run (() => EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Finaloffermade"], new RequestModel ()));

            const string EventName = "FinalOfferSucceed";
            await EventHub.Publish (EventName, new { FinalOffer = finalOfferDetails });

        }
        public string EnsureEntityType (string entityType) {
            if (string.IsNullOrWhiteSpace (entityType))
                throw new InvalidArgumentException ("Invalid Entity Type");

            entityType = entityType.ToLower ();
            var validEntityTypes = LookupService.GetLookupEntry ("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any ())
                throw new InvalidArgumentException ("Invalid Entity Type");

            return entityType;
        }
        public async Task<IEnumerable<IActivity>> GetActivities (string applicationNumber) {

            EnsureInputIsValid (applicationNumber);
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            var currentApplicationStatus = await EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId);
            if (currentApplicationStatus == null)
                throw new NotFoundException ($"Current Status of Application Number {applicationNumber} can not be found");

            return await Task.Run (() => EntityStatusService.GetActivities ("application", activeWorkflow.StatusWorkFlowId, currentApplicationStatus.Code));
        }

        public async Task ReviveApplication (string applicationNumber) {
            var result = string.Empty;
            EnsureInputIsValid (applicationNumber);
            try {
                Logger.Info ($"[statusmanagementservice] Get Application information for [{applicationNumber}]");
                var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);
                Logger.Info ($"[statusmanagementservice] Get Status of application for [{applicationNumber}]");
                var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
                var currentApplicationStatus = await EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId);
                if (application != null && currentApplicationStatus != null) {
                    var data = new { ApplicationStatus = currentApplicationStatus, Application = application };
                    object executionResult = DecisionEngineService.Execute<dynamic, dynamic> (
                        ApplicationProcessorConfiguration.ReviveApplicationRuleName,
                        new { payload = data });
                    Logger.Info ($"[statusmanagementservice] [{ApplicationProcessorConfiguration.ReviveApplicationRuleName}] rule executed to check eligibility of [{applicationNumber}]");
                    if (executionResult != null) {
                        result = GetResultValue (executionResult);
                    }
                }
                if (result != "Failed") {
                    var StatusHistory = await EntityStatusService.GetStatusTransitionHistory ("application", applicationNumber);
                    Logger.Info ($"[statusmanagementservice] get status history of [{applicationNumber}]");
                    var newStatus = StatusHistory.OrderByDescending (i => i.ActivedOn.Time).Skip (1).Take (1).FirstOrDefault ();
                    List<string> reason = newStatus.Reason;
                    await Task.Run (() => EntityStatusService.ChangeStatus ("application", applicationNumber, activeWorkflow.StatusWorkFlowId, newStatus.Status, new RequestModel () { reasons = reason })).ConfigureAwait (false);
                }
                var value = new { Date = DateTime.Now };
                await DataAttributesEngine.SetAttribute ("application", applicationNumber, "ApplicationRevived", value);
                await EventHub.Publish ("ApplicationRevived", new { EntityType = "application", EntityId = applicationNumber });
                Logger.Info ($"[statusmanagementservice] set attribute as [{applicationNumber}] is revived");
            } catch (Exception ex) {
                Logger.Error ($"Occurred an error: {ex.Message}\n", ex);
            }

        }

        private static string GetResultValue (dynamic data) {
            Func<dynamic> resultProperty = () => data.result;

            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }

        private static bool HasProperty<T> (Func<T> property) {
            try {
                property ();
                return true;
            } catch (Exception) {
                return false;
            }
        }

        private static T GetValue<T> (Func<T> property) {
            return property ();
        }

        private void EnsureInputIsValid (string entityId) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentException ($"{nameof(entityId)} is mandatory");

        }

        public static JToken FindToken<T> (string key, string value) {

            var jObject = JObject.Parse (value);
            var jToken = jObject.SelectToken (key);
            return jToken ?? null;
        }
    }
}