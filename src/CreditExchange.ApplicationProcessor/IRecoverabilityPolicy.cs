using System;

namespace CreditExchange.ApplicationProcessor
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}