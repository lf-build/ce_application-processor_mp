﻿using System;

namespace CreditExchange.ApplicationProcessor
{
    public class AlwaysRetryPolicy : IRecoverabilityPolicy
    {
        public bool CanBeRecoveredFrom(Exception ex)
        {
            return true;
        }
    }

}
