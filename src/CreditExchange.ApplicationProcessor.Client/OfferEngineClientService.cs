﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using RestSharp;

namespace CreditExchange.ApplicationProcessor.Client
{
    public class OfferEngineClientService : IOfferEngineService
    {
        public OfferEngineClientService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IFinalOfferDefination> GetFinalOffer(string applicationNumber,string filter = null)
        {
            var resourceRoute = string.IsNullOrEmpty(filter)? "/offer/{applicationNumber}/finaloffer" : "/offer/{applicationNumber}/finaloffer/{filter}";
            var request = new RestRequest(resourceRoute, Method.GET);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);
            if(!string.IsNullOrEmpty(filter))
                request.AddUrlSegment(nameof(filter), filter);
            return await Client.ExecuteAsync<FinalOfferDefination>(request);
        }

        public async Task<IInitialOfferDefination> GetInitialOffer(string applicationNumber)
        {
            var request = new RestRequest("/offer/{applicationNumber}/initialoffer", Method.GET);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);
            return await Client.ExecuteAsync<InitialOfferDefination>(request);
        }

        public Task<IInitialOfferDefination> InitiateInitialOffer(string applicationNumber,bool isReCompute)
        {
            throw new NotImplementedException();
        }

        public Task<IFinalOfferDefination> InitiateFinalOffer(string applicationNumber, bool isReCompute)
        {
            throw new NotImplementedException();
        }

        public Task SetSelectedInitialOffer(string applicationNumber, string offerId, bool isAccepted, List<string> reasons)
        {
            throw new NotImplementedException();
        }

        public Task SetSelectedFinalOffer(string applicationNumber, string offerId, bool isAccepted, List<string> reasons)
        {
            throw new NotImplementedException();
        }

        public Task UpdateCashFlow(string applicationNumber, object value)
        {
            throw new NotImplementedException();
        }

        public Task RejectInitialOffer(string applicationNumber, List<string> reasons)
        {
            throw new NotImplementedException();
        }

        public Task SelectInitialOffer(string applicationNumber, string offerId)
        {
            throw new NotImplementedException();
        }

        public Task RejectFinalOffer(string applicationNumber, List<string> reasons)
        {
            throw new NotImplementedException();
        }

        public Task SelectFinalOffer(string applicationNumber, string offerId)
        {
            throw new NotImplementedException();
        }

        public Task AddInitialOffer(string applicationNumber, IInitialOffer initialOffer)
        {
            throw new NotImplementedException();
        }

        public Task AddFinalOffer(string applicationNumber, IFinalOffer finalOffer)
        {
            throw new NotImplementedException();
        }

        public Task UpdateFinalOfferIntermediateData(string applicationnumber, string grade, string score)
        {
            throw new NotImplementedException();
        }

        public Task RepresentManualOffer(string applicationNumber, IFinalOffer finalOffer)
        {
            throw new NotImplementedException();
        }
        public async Task<bool> RepullCibil(string applicationNumber, string productId){
            throw new NotImplementedException();
        }
        
    }
}
