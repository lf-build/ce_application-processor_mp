﻿using System;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;

namespace CreditExchange.ApplicationProcessor.Client
{
    public class OfferEngineClientServiceFactory : IOfferEngineClientServiceFactory
    {
        public OfferEngineClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IOfferEngineService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new OfferEngineClientService(client);
        }
    }
}
