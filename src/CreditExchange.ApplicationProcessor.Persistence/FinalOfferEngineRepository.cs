﻿using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace CreditExchange.ApplicationProcessor.Persistence
{
    public class FinalOfferEngineRepository : MongoRepository<IFinalOfferDefination, FinalOfferDefination>, IFinalOfferEngineRepository
    {
        static FinalOfferEngineRepository()
        {
            BsonClassMap.RegisterClassMap<FinalOfferDefination>(map =>
            {
                map.AutoMap();
                var type = typeof(FinalOfferDefination);
                map.MapProperty(p => p.Status).SetSerializer(new EnumSerializer<ScoreCardApplicationStatus>(BsonType.String));

                map.MapProperty(m => m.OfferSelectionDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            if (!BsonClassMap.IsClassMapRegistered(typeof(TimeBucket)))
            {
                BsonClassMap.RegisterClassMap<TimeBucket>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                    var type = typeof(TimeBucket);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(false);
                });
            }
            BsonClassMap.RegisterClassMap<FinalOffer>(map =>
            {
                map.AutoMap();
                var type = typeof(FinalOffer);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public FinalOfferEngineRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "finaloffers")
        {
            CreateIndexIfNotExists("offer_id",
                Builders<IFinalOfferDefination>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityId));
        }

        public async Task<IFinalOfferDefination> GetFinalOffers(string entityType, string entityId, string filter =null)
        {
            var finaloffers = await Task.Run(() => Query.Where(i => i.EntityType == entityType && i.EntityId == entityId && (i.IsAvailable == true)).FirstOrDefault());
            if (!string.IsNullOrEmpty(filter) && finaloffers!=null)
                switch (filter.ToLower())
                {
                    case "selected":
                        finaloffers.FinalOffers = finaloffers.FinalOffers.Where(i => i.IsSelected == true).ToList();
                        break;
                    case "presented":
                        finaloffers.FinalOffers = finaloffers.FinalOffers.Where(i => i.IsPresented == true).ToList();
                        break;
                }
            return finaloffers;
        }
     
        public async Task<IFinalOffer> SetSelectedFinalOffer(string entityType, string entityId, string offerId, DateTimeOffset selectionDate)
        {
            var finalOffer = await GetFinalOffers(entityType, entityId);

            if (finalOffer == null)
                throw new NotFoundException($"Final offer not found for {entityId}");

            if (finalOffer.FinalOffers == null)
                throw new NotFoundException($"Final offer not found for {entityId}");

            var lstFinalOffers = finalOffer.FinalOffers;

            var currentOffer = lstFinalOffers.FirstOrDefault(offer => offer.OfferId.Equals(offerId));
            if (currentOffer == null)
                throw new NotFoundException($"Final offer with id { offerId } is not found");

            lstFinalOffers.ForEach(a => { a.IsSelected = false; });
            currentOffer.IsSelected = true;

            await Collection.UpdateOneAsync(Builders<IFinalOfferDefination>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                          a.EntityType == entityType && a.EntityId == entityId && (a.Status == (ScoreCardApplicationStatus.Completed) && a.IsAvailable == true)),
            Builders<IFinalOfferDefination>.Update.Set(a => a.FinalOffers, lstFinalOffers)
            .Set(a => a.OfferSelectionDate, selectionDate));
            return currentOffer;
        }
      
        public async Task AddFinalOffer(string entityType, string entityId, IFinalOffer offer)
        {
            var finalOffer = Query.FirstOrDefault(i => i.EntityType == entityType && i.EntityId == entityId && (i.Status == (ScoreCardApplicationStatus.Completed) || i.Status == (ScoreCardApplicationStatus.ManualReview)) && i.IsAvailable == true);

            if (finalOffer == null)
                throw new NotFoundException($"Fianl offer not found for {entityId}");

            if (finalOffer.FinalOffers == null)
                finalOffer.FinalOffers = new List<IFinalOffer>();

            var lstFinalOffers = finalOffer.FinalOffers;
            lstFinalOffers.Add(offer);

            await Collection.UpdateOneAsync(Builders<IFinalOfferDefination>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                          a.EntityType == entityType && a.EntityId == entityId && (a.Status == (ScoreCardApplicationStatus.Completed) || a.Status == (ScoreCardApplicationStatus.ManualReview)) && a.IsAvailable == true),
            Builders<IFinalOfferDefination>.Update.Set(a => a.FinalOffers, lstFinalOffers));
        }

        public async Task<IFinalOfferDefination> AddOffer(string entityType, string entityId, IFinalOfferDefination offer)
        {
            var filter = Builders<IFinalOfferDefination>.Filter.Where(i => i.EntityType == entityType && i.EntityId == entityId && i.TenantId == TenantService.Current.Id);
            var update = Builders<IFinalOfferDefination>.Update.Set(a => a.IsAvailable, false);

            var result = await Collection.UpdateManyAsync(filter, update);
            //offer.IsAvailable = true;
            Add(offer);
            return offer;
        }
    }
    
}
