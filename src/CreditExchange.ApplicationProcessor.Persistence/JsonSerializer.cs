﻿using System;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json;

namespace CreditExchange.ApplicationProcessor.Persistence
{
    public class JsonSerializer<T> : IBsonSerializer
    {
        public object Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var jsonString = context.Reader.ReadString();
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            var valueAsJson = JsonConvert.SerializeObject(value);
            context.Writer.WriteString(valueAsJson);
        }

        public Type ValueType => typeof(T);
    }
}
