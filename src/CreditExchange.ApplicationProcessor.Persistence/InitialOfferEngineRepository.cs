﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace CreditExchange.ApplicationProcessor.Persistence
{
    public class InitialOfferEngineRepository : MongoRepository<IInitialOfferDefination, InitialOfferDefination>, IInitialOfferEngineRepository
    {
        static InitialOfferEngineRepository()
        {

            BsonClassMap.RegisterClassMap<InitialOfferDefination>(map =>
           {
               map.AutoMap();
               var type = typeof(InitialOfferDefination);
               map.MapProperty(p => p.Status)
                   .SetSerializer(new EnumSerializer<ScoreCardApplicationStatus>(BsonType.String));

                //map.MapProperty(m => m.OfferSelectionDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
               map.SetIsRootClass(true);
           });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
           {
               map.AutoMap();
               map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

               var type = typeof(TimeBucket);
               map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
               map.SetIsRootClass(false);
           });
            BsonClassMap.RegisterClassMap<InitialOffer>(map =>
           {
               map.AutoMap();
               var type = typeof(InitialOffer);
               map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
               map.SetIsRootClass(true);
           });
        }
        public InitialOfferEngineRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "initialoffers")
        {
            CreateIndexIfNotExists("offer_id",
                Builders<IInitialOfferDefination>.IndexKeys.Ascending(i => i.TenantId)
                .Ascending(i => i.EntityId));
        }

        public async Task<IInitialOfferDefination> GetInitialOffers(string entityType, string entityId)
        {
            return await Task.Run(() => Query.Where(i => i.EntityType == entityType && i.EntityId == entityId && i.IsAvailable == true && i.Status == ScoreCardApplicationStatus.Completed).FirstOrDefault());
        }

        public async Task<IInitialOffer> SetSelectedInitialOffer(string entityType, string entityId, string offerId, DateTimeOffset selectionDate)
        {
            var initialOffer = Query.FirstOrDefault(i => i.EntityType == entityType && i.EntityId == entityId && i.Status == ScoreCardApplicationStatus.Completed && i.IsAvailable == true);

            if (initialOffer == null)
                throw new NotFoundException($"Initial offer not found for {entityId}");

            if (initialOffer.Offers == null)
                throw new NotFoundException($"Initial offer not found for {entityId}");

            var lstInitialOffers = initialOffer.Offers;

            var currentOffer = lstInitialOffers.FirstOrDefault(offer => offer.OfferId.Equals(offerId));
            if (currentOffer == null)
                throw new NotFoundException($"Initial offer with id { offerId } is not found");

            lstInitialOffers.ForEach(a => { a.IsSelected = false; });
            currentOffer.IsSelected = true;

            await Collection.UpdateOneAsync(Builders<IInitialOfferDefination>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                  a.EntityType == entityType && a.EntityId == entityId && a.Status == ScoreCardApplicationStatus.Completed && a.IsAvailable == true),
                Builders<IInitialOfferDefination>.Update.Set(a => a.Offers, lstInitialOffers)
                .Set(a => a.OfferSelectionDate, selectionDate));
            return currentOffer;
        }

        public async Task AddInitialOffer(string entityType, string entityId, IInitialOffer offer)
        {
            var initialOffer = Query.FirstOrDefault(i => i.EntityType == entityType && i.EntityId == entityId && i.Status == ScoreCardApplicationStatus.Completed && i.IsAvailable == true);

            if (initialOffer == null)
                throw new NotFoundException($"Initial offer not found for {entityId}");

            if (initialOffer.Offers == null)
                initialOffer.Offers = new List<IInitialOffer>();

            var lstInitialOffers = initialOffer.Offers;
            if (offer.IsSelected)
                lstInitialOffers.ForEach(a => { a.IsSelected = false; });

            lstInitialOffers.Add(offer);

            await Collection.UpdateOneAsync(Builders<IInitialOfferDefination>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                  a.EntityType == entityType && a.EntityId == entityId && a.Status == ScoreCardApplicationStatus.Completed && a.IsAvailable == true),
                Builders<IInitialOfferDefination>.Update.Set(a => a.Offers, lstInitialOffers));
        }

        public async Task<IInitialOfferDefination> AddOffer(string entityType, string entityId, IInitialOfferDefination offer)
        {
            // only if new genrated initialoffer is not failed mark old initial offer as available false.
            if (offer.Status.ToString() != "Failed")
            {
                var filter = Builders<IInitialOfferDefination>.Filter.Where(i => i.EntityType == entityType && i.EntityId == entityId && i.TenantId == TenantService.Current.Id);
                var update = Builders<IInitialOfferDefination>.Update.Set(a => a.IsAvailable, false);
                var result = await Collection.UpdateManyAsync(filter, update);
            }
            //offer.IsAvailable = true;
            Add(offer);
            return offer;
        }
    }
}