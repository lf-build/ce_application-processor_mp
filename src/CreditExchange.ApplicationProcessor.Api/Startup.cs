﻿using CreditExchange.Application.Client;
using CreditExchange.ApplicationProcessor.Persistence;
using CreditExchange.CompanyDb.Client;
using LendFoundry.Consent.Client;
using LendFoundry.DataAttributes.Client;
// using CreditExchange.EmailHunter.Client;
using System.IO;
using CreditExchange.Applicant.Client;
using CreditExchange.Applications.Filters.Client;
using CreditExchange.Cibil.Client;
using CreditExchange.GeoProfile.Client;
using CreditExchange.Lenddo.Client;
using CreditExchange.Perfios.Client;
using CreditExchange.ViewDNS.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration.Client;
using LendFoundry.EmailVerification.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tenant.Client;
using LendFoundry.VerificationEngine.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
// using CreditExchange.Towedata.Client;
using LendFoundry.AlertEngine.Client;
using LendFoundry.Application.Document.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.ProductRule.Client;

namespace CreditExchange.ApplicationProcessor.Api {
    public class Startup {
        public void ConfigureServices (IServiceCollection services) {
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", new Info {
                    Version = "v1",
                        Title = "ApplicationProcessor"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme () {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "CreditExchange.ApplicationProcessor.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();
            services.AddTenantTime ();
            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddEventHub (Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService (Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddConfigurationService<ApplicationProcessorConfiguration> (Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);

            services.AddSingleton<IMongoConfiguration> (p => new MongoConfiguration (Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient (provider => provider.GetRequiredService<IConfigurationService<ApplicationProcessorConfiguration>> ().Get ());

            services.AddGeoProfileService (Settings.GeoProfile.Host, Settings.GeoProfile.Port);
            services.AddApplicantDocumentService (Settings.ApplicationDocument.Host, Settings.ApplicationDocument.Port);
            services.AddPerfiosService (Settings.Perfios.Host, Settings.Perfios.Port);
            services.AddLenddoService (Settings.Lenddo.Host, Settings.Lenddo.Port);
            services.AddCompanyService (Settings.CompanyService.Host, Settings.CompanyService.Port);
            services.AddDataAttributes (Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            services.AddLookupService (Settings.LookUpService.Host, Settings.LookUpService.Port);
            services.AddStatusManagementService (Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddConsentService (Settings.ConsentService.Host, Settings.ConsentService.Port);
            services.AddApplicationService (Settings.Application.Host, Settings.Application.Port);
            services.AddApplicantService (Settings.Applicant.Host, Settings.Applicant.Port);
            services.AddDecisionEngine (Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddVerificationEngineService (Settings.VerificationEngine.Host, Settings.VerificationEngine.Port);
            //services.AddEmailHunterService(Settings.EmailHunterService.Host, Settings.EmailHunterService.Port);
            services.AddViewDnsService (Settings.ViewDnsService.Host, Settings.ViewDnsService.Port);
            services.AddEmailVerificationService (Settings.EmailVerification.Host, Settings.EmailVerification.Port);
            services.AddApplicationsFilterService (Settings.ApplicationsFilterService.Host, Settings.ApplicationsFilterService.Port);
            services.AddCibilReportService (Settings.CibilService.Host, Settings.CibilService.Port);
            services.AddApplicantService (Settings.Applicant.Host, Settings.Applicant.Port);
            //services.AddTowerdataService(Settings.TowerData.Host, Settings.TowerData.Port);
            services.AddAlertEngine (Settings.AlertEngine.Host, Settings.AlertEngine.Port);
            services.AddDocumentGenerator (Settings.DocumentGenerator.Host, Settings.DocumentGenerator.Port);
            services.AddProductRuleService (Settings.ProductService.Host, Settings.ProductService.Port);

            services.AddTransient<IInitialOfferEngineRepository, InitialOfferEngineRepository> ();
            services.AddTransient<IOrchestrationService, OrchestrationService> ();
            services.AddTransient<IInitialOffer, InitialOffer> ();
            services.AddTransient<IFinalOfferEngineRepository, FinalOfferEngineRepository> ();
            services.AddTransient<IOfferEngineService, OfferEngineService> ();
            services.AddTransient<IApplicationService, ApplicationService> ();
            services.AddTransient<IStatusManagementService, StatusManagementService> ();
            services.AddMvc ().AddLendFoundryJsonOptions ();
            services.AddCors ();
        }

        public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILogger loggerFactory) {
            app.UseSwagger ();
            app.UseHealthCheck ();
            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "ApplicationProcessor Service");
            });
            app.UseCors (env);
            app.UseErrorHandling ();
            app.UseRequestLogging ();
            app.UseMvc ();
        }
    }
    //public class RequestLoggingMiddleware
    //{
    //    private RequestDelegate Next { get; }

    //    public RequestLoggingMiddleware(RequestDelegate next)
    //    {
    //        Next = next;
    //    }

    //    public async Task Invoke(HttpContext context)
    //    {
    //        //The MVC middleware reads the body stream before we do, which
    //        //means that the body stream's cursor would be positioned at the end.
    //        // We need to reset the body stream cursor position to zero in order
    //        // to read it. As the default body stream implementation does not
    //        // support seeking, we need to explicitly enable rewinding of the stream:
    //        try
    //        {
    //            await Next.Invoke(context);
    //        }
    //        catch (Exception exception)
    //        {

    //        }
    //    }
    //}
}