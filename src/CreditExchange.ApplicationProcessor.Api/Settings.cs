﻿using System;
using LendFoundry.Foundation.Services.Settings;

namespace CreditExchange.ApplicationProcessor.Api {
    public static class Settings {
        public static string ServiceName { get; } = "offer-engine";

        private static string Prefix { get; } = ServiceName.ToUpper ();

        public static ServiceSettings Configuration { get; } = new ServiceSettings ($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings ($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Tenant { get; } = new ServiceSettings ($"{Prefix}_TENANT", "tenant");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings ($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "offer-engine");

        //AddGeoProfileService
        public static ServiceSettings GeoProfile { get; } = new ServiceSettings ($"{Prefix}_GEOPROFILE_HOST", "geoprofile", $"{Prefix}_GEOPROFILE_PORT");

        //AddCrifCreditReportServiceExtensions
        public static ServiceSettings CrifCreditReport { get; } = new ServiceSettings ($"{Prefix}_CRIFCREDITREPORT_HOST", "syndication-crif-credit-report", $"{Prefix}_CRIFCREDITREPORT_PORT");

        //AddCrifPanVerificationServiceExtensions
        public static ServiceSettings CrifPanVerification { get; } = new ServiceSettings ($"{Prefix}_CRIFPANVERIFICATION_HOST", "syndication-crif-pan-verification", $"{Prefix}_CRIFPANVERIFICATION_PORT");

        //AddApplicationService
        public static ServiceSettings Application { get; } = new ServiceSettings ($"{Prefix}_APPLICATION_HOST", "application", $"{Prefix}_APPLICATION_PORT");

        //AddApplicantService
        public static ServiceSettings Applicant { get; } = new ServiceSettings ($"{Prefix}_APPLICANT_HOST", "applicant", $"{Prefix}_APPLICANT_PORT");

        //AddDecisionEngine
        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings ($"{Prefix}_DECISIONENGINE_HOST", "decision-engine", $"{Prefix}_DECISIONENGINE_PORT");

        //AddPerfiosService
        public static ServiceSettings Perfios { get; } = new ServiceSettings ($"{Prefix}_PERFIOS_HOST", "syndication-perfios", $"{Prefix}_PERFIOS_PORT");

        //AddLenddoService
        public static ServiceSettings Lenddo { get; } = new ServiceSettings ($"{Prefix}_LENDDO_HOST", "syndication-lenddo", $"{Prefix}_LENDDO_PORT");
        public static ServiceSettings CompanyService { get; } = new ServiceSettings ($"{Prefix}_COMPANYDB_HOST", "company-db", $"{Prefix}_COMPANYDB_PORT");
        public static ServiceSettings DataAttributes { get; } = new ServiceSettings ($"{Prefix}_DATAATTRIBUTES_HOST", "data-attributes", $"{Prefix}_DATAATTRIBUTES_PORT");
        public static ServiceSettings ScoreCard { get; } = new ServiceSettings ($"{Prefix}_SCORECARD_HOST", "scorecard", $"{Prefix}_SCORECARD_PORT");
        public static ServiceSettings LookUpService { get; } = new ServiceSettings ($"{Prefix}_LOOKUP_HOST", "lookup-service", $"{Prefix}_LOOKUP_PORT");
        public static ServiceSettings StatusManagement { get; } = new ServiceSettings ($"{Prefix}_STATUSMANAGEMENT_HOST", "status-management", $"{Prefix}_STATUSMANAGEMENT_PORT");

        public static ServiceSettings EmailHunterService { get; } = new ServiceSettings ($"{Prefix}_EMAILHUNTER_HOST", "syndication-email-hunter", $"{Prefix}_EMAILHUNTER_PORT");
        public static ServiceSettings ConsentService { get; } = new ServiceSettings ($"{Prefix}_APPLICATION-CONSENT_HOST", "application-consent", $"{Prefix}_APPLICATION-CONSENT_PORT");
        public static ServiceSettings ViewDnsService { get; } = new ServiceSettings ($"{Prefix}_VIEWDNS_HOST", "syndication-view-dns", $"{Prefix}_VIEWDNS_PORT");
        public static ServiceSettings VerificationEngine { get; } = new ServiceSettings ($"{Prefix}_VERIFICATIONENGINE_HOST", "verification-engine", $"{Prefix}_VERIFICATIONENGINE_PORT");
        public static ServiceSettings EmailVerification { get; } = new ServiceSettings ($"{Prefix}_EMAILOTP_HOST", "email-verification", $"{Prefix}_EMAILOTP_PORT");
        public static ServiceSettings ApplicationsFilterService { get; } = new ServiceSettings ($"{Prefix}_APPLICATIONFILTERS_HOST", "application-filters", $"{Prefix}_APPLICATIONFILTERS_PORT");

        public static ServiceSettings CibilService { get; } = new ServiceSettings ($"{Prefix}_CIBIL_HOST", "cibil", $"{Prefix}_CIBIL_PORT");

        public static ServiceSettings ApplicationDocument { get; } = new ServiceSettings ($"{Prefix}_APPLICATIONDOCUMENT", "application-document");

        public static ServiceSettings TowerData { get; } = new ServiceSettings ($"{Prefix}_TOWERDATA_HOST", "syndication-towerdata", $"{Prefix}_TOWERDATA_PORT");

        public static ServiceSettings AlertEngine { get; } = new ServiceSettings ($"{Prefix}_ALERTENGINE_HOST", "alertengine", $"{Prefix}_ALERTENGINE_PORT");

        public static ServiceSettings Yodlee { get; } = new ServiceSettings ($"{Prefix}_Yodlee_HOST", "Yodlee", $"{Prefix}_Yodlee_PORT");

        public static ServiceSettings DocumentGenerator { get; } = new ServiceSettings ($"{Prefix}_DocumentGenerator_Host", "document-generator", $"{Prefix}_DocumentGenerator_Port");
        public static string Nats => Environment.GetEnvironmentVariable ($"{Prefix}_NATS_URL") ?? "nats";
        public static ServiceSettings ProductService { get; } = new ServiceSettings ($"{Prefix}_PRODUCTRULE_HOST", "productrule", $"{Prefix}_PRODUCTRULE_PORT");

    }
}