﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.ApplicationProcessor.Api.Controllers
{
    [Route("/offer")]
    public class OfferEngineController : ExtendedController
    {
        public OfferEngineController(IOfferEngineService offerEngineService, ILogger logger) : base(logger)
        {
            if (offerEngineService == null)
            {
                throw new ArgumentNullException(nameof(offerEngineService));
            }

            OfferEngineService = offerEngineService;

        }

        private IOfferEngineService OfferEngineService { get; }

        [HttpPost("{applicationNumber}/initialoffer")]
        [ProducesResponseType(typeof(IInitialOfferDefination), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> InitiateInitialOffer(string applicationNumber)
        {
            Logger.Info($"Initial offer endpoint called for [{applicationNumber}]");
            return await this.ExecuteAsync(async () => this.Ok(await this.OfferEngineService.InitiateInitialOffer(applicationNumber, true).ConfigureAwait(false))).ConfigureAwait(false);
            
        }

        /// <summary>
        /// The get initial offer.
        /// </summary>
        /// <param name="applicationNumber">
        /// The application number.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet("{applicationNumber}/initialoffer")]
        [ProducesResponseType(typeof(IInitialOfferDefination), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetInitialOffer(string applicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await OfferEngineService.GetInitialOffer(applicationNumber).ConfigureAwait(false))).ConfigureAwait(false);
            }
            catch (NotFoundException notFoundException)
            {
                Logger.Info(notFoundException.Message);
                return NotFound(notFoundException.Message);

            }
        }

        [HttpPost("{applicationNumber}/reGenerateInitialoffer/{reCallSyndication}")]
        [ProducesResponseType(typeof(IInitialOfferDefination), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ReComputeInitialOffer(string applicationNumber, bool reCallSyndication)
        {
            return await ExecuteAsync(async () => Ok(await OfferEngineService.InitiateInitialOffer(applicationNumber, reCallSyndication).ConfigureAwait(false))).ConfigureAwait(false);
        }

        [HttpPost("{applicationNumber}/finaloffer")]
        [ProducesResponseType(typeof(IFinalOfferDefination), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> InitiateFinalOffer(string applicationNumber) //([FromRoute]string applicationNumber)
            => await ExecuteAsync(async () => Ok((await OfferEngineService.InitiateFinalOffer(applicationNumber, true))));

        [HttpPost("{applicationNumber}/reGenerateFinaloffer/{reCallSyndication}")]
        [ProducesResponseType(typeof(IFinalOfferDefination), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]

        public async Task<IActionResult> ReGenerateFinaloffer(string applicationNumber, bool reCallSyndication)
        {
            return await ExecuteAsync(async () => Ok(await OfferEngineService.InitiateFinalOffer(applicationNumber, reCallSyndication).ConfigureAwait(false))).ConfigureAwait(false);
        }

        [HttpGet("{applicationNumber}/finaloffer")]
        [ProducesResponseType(typeof(IFinalOfferDefination), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetFinalOffer(string applicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await OfferEngineService.GetFinalOffer(applicationNumber).ConfigureAwait(false))).ConfigureAwait(false);
            }
            catch (NotFoundException notFoundException)
            {
                Logger.Info(notFoundException.Message);
                return NotFound(notFoundException.Message);
            }
        }
        [HttpGet("{applicationNumber}/finaloffer/{filter}")]
        [ProducesResponseType(typeof(IFinalOfferDefination), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetFinalOffer(string applicationNumber,string filter)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await OfferEngineService.GetFinalOffer(applicationNumber,filter).ConfigureAwait(false))).ConfigureAwait(false);
            }
            catch (NotFoundException notFoundException)
            {
                Logger.Info(notFoundException.Message);
                return NotFound(notFoundException.Message);
            }
        }

        [HttpPut("{applicationNumber}/acceptinitialoffer/{offerId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> SelectInitialOffer(string applicationNumber, string offerId)
        {
            return await ExecuteAsync(async () =>
            {
                await OfferEngineService.SelectInitialOffer(applicationNumber, offerId);
                return new NoContentResult();
            });
        }

        [HttpPut("{applicationNumber}/rejectinitialoffer/{*reasons}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> RejectInitialOffer(string applicationNumber, string reasons)
        {
            return await ExecuteAsync(async () =>
            {
                await OfferEngineService.RejectInitialOffer(applicationNumber, SplitString(reasons));
                return new NoContentResult();
            });
        }

        [HttpPut("{applicationNumber}/acceptfinaloffer/{offerId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> SelectFinalOffer(string applicationNumber, string offerId)
        {
            return await ExecuteAsync(async () =>
            {
                await OfferEngineService.SelectFinalOffer(applicationNumber, offerId);
                return new NoContentResult();
            });
        }
      
        /// <summary>
        /// The reject final offer.
        /// </summary>
        /// <param name="applicationNumber">
        /// The application number.
        /// </param>
        /// <param name="reasons">
        /// The reasons.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPut("{applicationNumber}/rejectfinaloffer/{*reasons}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> RejectFinalOffer(string applicationNumber, string reasons)
        {
            return await ExecuteAsync(async () =>
            {
                await OfferEngineService.RejectFinalOffer(applicationNumber, SplitString(reasons));
                return new NoContentResult();
            });
        }

        //[HttpPut("{applicationNumber}/addmanualinitialoffer")]
        //public async Task<IActionResult> AddInitialOffer(string applicationNumber, [FromBody]InitialOffer initialOffer)
        //{
        //    return await ExecuteAsync(async () =>
        //    {
        //        await OfferEngineService.AddInitialOffer(applicationNumber,initialOffer);
        //        return new NoContentResult();
        //    });
        //}

        [HttpPut("{applicationNumber}/addmanualfinaloffer")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> AddFinalOffer(string applicationNumber, [FromBody]FinalOffer finalOffer)
        {
            return await ExecuteAsync(async () =>
            {
                await OfferEngineService.AddFinalOffer(applicationNumber, finalOffer);
                return new NoContentResult();
            });
        }
        //[HttpPut("{applicationNumber}/finaloffer/{offerId}/{isAccepted}/{*reasons}")]
        //public async Task<IActionResult> SetSelectedFinalOffer(string applicationNumber, string offerId, bool isAccepted, string reasons) =>
        //   await ExecuteAsync(async () => { await OfferEngineService.SetSelectedFinalOffer(applicationNumber, offerId, isAccepted, SplitString(reasons)); return new NoContentResult(); });

        private static List<string> SplitString(string reasons)
        {
            return string.IsNullOrWhiteSpace(reasons)
                ? new List<string>()
                : reasons.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        //TODO: Move to Application controller
        [HttpPost("{applicationNumber}/cashflow")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateCashFlow(string applicationNumber, [FromBody]object value)
        {
            return await ExecuteAsync(async () =>
            {
                await OfferEngineService.UpdateCashFlow(applicationNumber, value);
                return Ok();
            });
        }
        [HttpPost("{applicationnumber}/{grade}/{score}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateFinalOfferIntermediateData(string applicationnumber, string grade, string score)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    await OfferEngineService.UpdateFinalOfferIntermediateData(applicationnumber, grade, score);
                    return Ok();
                }
                catch (Exception ex)
                {
                    return new ErrorResult(500, ex.Message);
                }
            });
           
        }

        [HttpPut("{applicationnumber}/representmanualoffer")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> RepresentManualOffer(string applicationNumber, [FromBody]FinalOffer finalOffer)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    await OfferEngineService.RepresentManualOffer(applicationNumber, finalOffer);
                    return Ok();
                }
                catch (Exception ex)
                {
                    return new ErrorResult(500, ex.Message);
                }
            });
        }

                /// <summary>
        /// RepullCibil - for repull cibil data
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{applicationNumber}/{productId}/repullCibil")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> RepullCibil(string applicationNumber, string productId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    await OfferEngineService.RepullCibil(applicationNumber, productId);
                    return Ok();
                }
                catch (Exception ex)
                {
                    return new ErrorResult(500, ex.Message);
                }
            });
        }
    }
}
