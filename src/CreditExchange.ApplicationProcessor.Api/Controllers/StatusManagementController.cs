﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace CreditExchange.ApplicationProcessor.Api.Controllers
{
    [Route("/status-management")]
    public class StatusManagementController : ExtendedController
    {

        public StatusManagementController(IStatusManagementService statusManagementService)
        {
            if (statusManagementService == null)
                throw new ArgumentNullException(nameof(statusManagementService));

            StatusManagementService = statusManagementService;
        }

        private IStatusManagementService StatusManagementService { get; set; }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        [HttpPost("{applicationNumber}/{newStatus}/{*reasons}")]
         [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ChangeStatus(string applicationNumber, string newStatus, string reasons)
        {
            return await ExecuteAsync(async () =>
            {
                await StatusManagementService.ChangeStatus(applicationNumber, newStatus, Reasons(reasons));
                return Ok();
            });
        }

        [HttpPost("{applicationNumber}/rejectApplication/{*reasons}")]
         [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> RejectApplication(string applicationNumber, string reasons)
        {
            return await ExecuteAsync(async () =>
            {
                await StatusManagementService.RejectApplication(applicationNumber, Reasons(reasons));
                return Ok();
            });
        }


        [HttpPost("{applicationNumber}/applicationapproved")]
         [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ApplicationStatusMove(string applicationNumber)
        {
            return await ExecuteAsync(async () =>
            {
                await StatusManagementService.ApplicationStatusMove(applicationNumber);
                return Ok();
            });
        }
        

        [HttpPost("{applicationNumber}/manualreview-finaloffermade")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> FinalOfferReadyFromManualReview(string applicationNumber,[FromBody]object offerId)
        {
            return await ExecuteAsync(async () =>
            {
                await StatusManagementService.FinalOfferReadyFromManualReview(applicationNumber,offerId);
                return Ok();
            });
        }

        [HttpGet("{applicationNumber}/activities")]
        [ProducesResponseType(typeof(IEnumerable<LendFoundry.StatusManagement.IActivity>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetActivities(string applicationNumber)
        {
            return await ExecuteAsync(async () =>
            {

                return Ok(await StatusManagementService.GetActivities(applicationNumber));
            });
        }

        [HttpPut("{applicationNumber}/reviveapplication")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ReviveApplication(string applicationNumber)
        {
            return await ExecuteAsync(async () => { await StatusManagementService.ReviveApplication(applicationNumber).ConfigureAwait(false); return NoContentResult; }).ConfigureAwait(false);
        }
        private static IEnumerable<string> SplitReasons(string reasons)
        {
            return string.IsNullOrWhiteSpace(reasons)
                ? new List<string>()
                : reasons.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }


        private static List<string> Reasons(string tags)
        {
            if (string.IsNullOrWhiteSpace(tags))
                return null;

            tags = WebUtility.UrlDecode(tags);
            var tagList = SplitReasons(tags);
            return tagList.ToList();
        }
    }
}
