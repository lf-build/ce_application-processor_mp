﻿using System;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CreditExchange.Applicant;
using CreditExchange.Application;
using CreditExchange.Applications.Filters.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp.Extensions;

namespace CreditExchange.ApplicationProcessor.Api.Controllers {
    [Route ("/application")]
    public class ApplicationController : ExtendedController {
        public ApplicationController (IApplicationService applicationService, ILogger logger) : base (logger) {
            if (applicationService == null) {
                throw new ArgumentNullException (nameof (applicationService));
            }
            ApplicationService = applicationService;
        }

        private IApplicationService ApplicationService { get; }
        private static NoContentResult NoContentResult { get; } = new NoContentResult ();

        [HttpGet]
        [ProducesResponseType (typeof (SubmitApplicationRequest), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public IActionResult GetObject () {
            return Ok (new SubmitApplicationRequest ());
        }

        [HttpPost ("submit")]
        [ProducesResponseType (typeof (Tuple<IApplicationResponse, string>), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> SubmitApplication ([FromBody] SubmitApplicationRequest submitApplicationRequest) {
            return await ExecuteAsync (async () => {
                var applicationResponse = await ApplicationService.SubmitApplication (submitApplicationRequest);
                if (applicationResponse.Item2 == "ApplicationSubmitted")
                    return Ok (applicationResponse.Item1);
                else {
                    ObjectResult response = new ObjectResult (applicationResponse.Item1);
                    response.StatusCode = 202;
                    return response;
                }
            });
        }

        /// <summary>
        /// The add lead.
        /// </summary>
        /// <param name="submitApplicationRequest">
        /// The submit application request.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost ("lead")]
        [ProducesResponseType (typeof (IApplicationResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> AddLead ([FromBody] SubmitApplicationRequest submitApplicationRequest) {
            return await ExecuteAsync (async () => Ok (await ApplicationService.AddLead (submitApplicationRequest).ConfigureAwait (false))).ConfigureAwait (false);
        }

        [HttpPut ("{applicationNumber}/employmentDetail")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateEmploymentDetail (string applicationNumber, [FromBody] EmploymentDetail employmentDetail) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdateEmploymentDetail (applicationNumber, employmentDetail).ConfigureAwait (false); return NoContentResult; }).ConfigureAwait (false);
        }

        [HttpPut ("{applicationNumber}/selfDeclaredExpense")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateSelfDeclaredExpense (string applicationNumber, [FromBody] ExpenseDetailRequest selfDeclaredExpense) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdateSelfDeclaredExpense (applicationNumber, selfDeclaredExpense).ConfigureAwait (false); return NoContentResult; }).ConfigureAwait (false);
        }

        [HttpPut ("{applicationNumber}/bankInformation")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateBankInformation (string applicationNumber, [FromBody] BankInformation bankInformation) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdateBankInformation (applicationNumber, bankInformation).ConfigureAwait (false); return NoContentResult; }).ConfigureAwait (false);
        }

        [HttpPut ("{applicationNumber}/educationInformation")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateEducationInformation (string applicationNumber, [FromBody] EducationInformation educationInformation) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdateEducationInformation (applicationNumber, educationInformation).ConfigureAwait (false); return NoContentResult; }).ConfigureAwait (false);
        }

        [HttpPut ("{applicationNumber}/emailAddress")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateEmailInformation (string applicationNumber, [FromBody] EmailAddress emailInforamtion) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdateEmailInformation (applicationNumber, emailInforamtion).ConfigureAwait (false); return NoContentResult; }).ConfigureAwait (false);
        }

        [HttpPut ("{applicationNumber}/reInitiateEmail")]
        [ProducesResponseType (typeof (IEmailAddress), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> ReInitiateEmailVerification (string applicationNumber, [FromBody] EmailAddress emailInforamtion) {

            return await ExecuteAsync (async () => Ok (await ApplicationService.ReInitiateEmailVerification (applicationNumber, emailInforamtion).ConfigureAwait (false))).ConfigureAwait (false);

        }

        [HttpPost ("{applicationNumber}/ancillarydata")]
        [ProducesResponseType (typeof (IAncillaryData), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateAncillaryData (string applicationNumber, [FromBody] AncillaryData ancillaryData) {
            return await ExecuteAsync (async () => Ok (await ApplicationService.UpdateAncillaryData (applicationNumber, ancillaryData).ConfigureAwait (false))).ConfigureAwait (false);
        }

        [HttpPost ("{applicationNumber}/loan-fundeddata")]
        [ProducesResponseType (typeof (ILoanFundedData), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateLoanFundedData (string applicationNumber, [FromBody] LoanFundedData loanFundedData) {
            return await ExecuteAsync (async () => Ok (await ApplicationService.UpdateLoanFundedData (applicationNumber, loanFundedData).ConfigureAwait (false))).ConfigureAwait (false);
        }

        [HttpGet ("{applicationNumber}/bank-preference")]
        [ProducesResponseType (typeof (IBankSourceData), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> GetBankSourceData (string applicationNumber) {
            return await ExecuteAsync (async () => Ok (await ApplicationService.GetBankSourceData (applicationNumber).ConfigureAwait (false))).ConfigureAwait (false);
        }

        [HttpPost ("{applicationNumber}/perfios-reportData")]
        [ProducesResponseType (typeof (object), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdatePerfiosData (string applicationNumber, [FromBody] object value) {
            return await ExecuteAsync (async () => Ok (await ApplicationService.UpdatePerfiosData (applicationNumber, value).ConfigureAwait (false))).ConfigureAwait (false);
        }

        [HttpPost ("{applicationNumber}/{bankSource}/bank-preference")]
        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateBankSourceData (string applicationNumber, string bankSource) {
            return await ExecuteAsync (async () => Ok (await ApplicationService.UpdateBankSourceData (applicationNumber, bankSource).ConfigureAwait (false))).ConfigureAwait (false);
        }

        [HttpPost ("{applicationNumber}/perfios-upload-statement")]
        [ProducesResponseType (typeof (CreditExchange.Syndication.Perfios.Response.IUploadStatementResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> perfiosuploadstatement (string applicationNumber, string password,
            IFormFile file) {
            return await ExecuteAsync (async () => {
                try {
                    if (file == null || file.Length <= 0)
                        return ErrorResult.BadRequest ("Please upload file.");
                    var fileName = ContentDispositionHeaderValue
                        .Parse (file.ContentDisposition)
                        .FileName
                        .Trim ('"');

                    return await ExecuteAsync (async () => Ok (await ApplicationService.UploadBankStatement ("application", applicationNumber, fileName, file.OpenReadStream ().ReadAsBytes (), password)));
                } catch (BankStatementUploadFailedException exception) {
                    return ErrorResult.BadRequest (exception.Message);
                }
            });
        }

        [HttpPut ("{applicationNumber}/update-application")]
        [ProducesResponseType (typeof (Tuple<IApplication, string>), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateApplication (string applicationNumber, [FromBody] SubmitApplicationRequest applicationRequest) {
            try {
                Logger.Info ($"[OfferEngine/ApplicationService] UpdateApplication end point called for [{applicationNumber}] request : [{(applicationRequest != null ? JsonConvert.SerializeObject(applicationRequest) : null)}] ");
                return await ExecuteAsync (async () => {
                    var applicationResponse = await ApplicationService.UpdateApplication (applicationNumber, applicationRequest);
                    if (applicationResponse.Item2 == "ApplicationSubmitted")
                        return Ok (applicationResponse.Item1);
                    else {
                        ObjectResult response = new ObjectResult (applicationResponse.Item1);
                        response.StatusCode = 202;
                        return response;
                    }
                });
            } catch (Exception ex) {
                Logger.Info ($"[OfferEngine/ApplicationService] UpdateApplication end point called failed for [{applicationNumber}] error : [{ex.Message}] ");
                return ErrorResult.BadRequest (ex.Message);
            }
        }

        [HttpPut ("{applicationNumber}/applicantDetails")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdatePersonalDetails (string applicationNumber, [FromBody] UpdateApplicantRequest personalDetails) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdatePersonalDetails (applicationNumber, personalDetails); return NoContentResult; });
        }

        [HttpPost ("{applicationNumber}/changeStatus-submit")]
        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateStatus (string applicationNumber) {
            return await ExecuteAsync (async () => Ok (await ApplicationService.UpdateStatus (applicationNumber).ConfigureAwait (false))).ConfigureAwait (false);
        }

        [HttpPut ("{applicationNumber}/address")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateAddress (string applicationNumber, [FromBody] Address address) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdateAddress (applicationNumber, address).ConfigureAwait (false); return NoContentResult; }).ConfigureAwait (false);
        }

        [HttpPost ("{applicationnumber}/{applicantId}/EnableReApplyInCoolOff")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> EnableReApplyInCoolOff (string applicationnumber, string applicantId) {
            return await ExecuteAsync (async () => { await ApplicationService.EnableReApplyInCoolOff (applicationnumber, applicantId); return NoContentResult; });
        }

        [HttpPut ("{applicationnumber}/requestLoanAmount")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateLoanAmount (string applicationnumber, [FromBody] UpdateLoanAmountRequest requestLoanAmount) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdateLoanAmount (applicationnumber, requestLoanAmount).ConfigureAwait (false); return NoContentResult; }).ConfigureAwait (false);
        }

        [HttpPut ("requestLoanPurpose/{applicationNumber}/{loanpurpose}/{otherloanpurpose?}")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateLoanPurpose (string applicationNumber, string loanPurpose, string otherLoanPurpose = null) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdateLoanPurpose (applicationNumber, loanPurpose, otherLoanPurpose); return NoContentResult; }).ConfigureAwait (false);
        }

        [HttpGet ("EligibilityCheckForReApply/{parametertype}/{parametervalue}")]
        [ProducesResponseType (typeof (bool), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> EligibilityCheckForReApply (UniqueParameterTypes parametertype, string parametervalue) {
            try {
                return await ExecuteAsync (async () => {
                    return Ok (!(await ApplicationService.CheckIfDuplicateApplicationExists (parametertype, parametertype == UniqueParameterTypes.Email ? WebUtility.UrlDecode (parametervalue) : parametervalue, string.Empty)));
                });
            } catch (Exception ex) {
                return new ErrorResult (500, ex.Message);
            }
        }

        [HttpGet ("verify-email/{email}")]
        [ProducesResponseType (typeof (bool), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> VerifyEmail (string email) {
            try {
                return await ExecuteAsync (async () => {
                    return Ok (await ApplicationService.IsValidEmail (email));
                });
            } catch (Exception ex) {
                return new ErrorResult (500, ex.Message);
            }

        }

        [HttpPut ("updateaadhaar/{applicationnumber}/{aadhaarnumber}")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateAadhaarNumber (string applicationnumber, string aadhaarnumber) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdateAadhaarnumber (applicationnumber, aadhaarnumber).ConfigureAwait (false); return NoContentResult; }).ConfigureAwait (false);
        }

        [HttpPost ("lead/newborrowerportal")]
        [ProducesResponseType (typeof (IApplicationResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> AddLeadForNewBorrowerPortal ([FromBody] SubmitApplicationRequest submitApplicationRequest) {
            return await ExecuteAsync (async () => Ok (await ApplicationService.AddLeadForNewBorrowerPortal (submitApplicationRequest).ConfigureAwait (false))).ConfigureAwait (false);
        }

        [HttpPut ("{applicationNumber}/update-lead-application")]
        [ProducesResponseType (typeof (Tuple<IApplication, string>), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdatePartialApplicationData (string applicationNumber, [FromBody] SubmitApplicationRequest applicationRequest) {
            try {
                Logger.Info ($"[OfferEngine/ApplicationService] Update Lead Application end point called for [{applicationNumber}] request : [{(applicationRequest != null ? JsonConvert.SerializeObject(applicationRequest) : null)}] ");
                return await ExecuteAsync (async () => {
                    var applicationResponse = await ApplicationService.UpdatePartialApplicationData (applicationNumber, applicationRequest);
                    if (applicationResponse.Item2 == "ApplicationSubmitted")
                        return Ok (applicationResponse.Item1);
                    else {
                        ObjectResult response = new ObjectResult (applicationResponse.Item1);
                        response.StatusCode = 202;
                        return response;
                    }
                });
            } catch (Exception ex) {
                Logger.Info ($"[OfferEngine/ApplicationService] Update Lead Application end point called failed for [{applicationNumber}] error : [{ex.Message}] ");
                return ErrorResult.BadRequest (ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="employmentDetail"></param>
        /// <param name="employementYear"></param>
        /// <param name="employementMonth"></param>
        /// <returns>void</returns>
        [HttpPut ("{applicationNumber}/employmentDetail/{employementYear}/{employementMonth}")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateEmploymentDetail (string applicationNumber, [FromBody] EmploymentDetail employmentDetail, string employementYear = null, string employementMonth = null) {
            return await ExecuteAsync (async () => { await ApplicationService.UpdateEmploymentDetail (applicationNumber, employmentDetail, employementYear, employementMonth).ConfigureAwait (false); return NoContentResult; }).ConfigureAwait (false);
        }

        /// <summary>
        /// SwitchProductfromQAFilter
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="newProductId"></param>
        /// <returns></returns>
        [HttpPut ("switchproduct/{applicationNumber}/{newProductId}")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> SwitchProductfromQAFilter (string applicationNumber, string newProductId) {
            return await ExecuteAsync (async () => { await ApplicationService.SwitchProductfromQAFilter (applicationNumber, newProductId).ConfigureAwait (false); return NoContentResult; }).ConfigureAwait (false);
        }

    }
}