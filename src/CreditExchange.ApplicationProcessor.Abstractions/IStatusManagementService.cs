﻿using LendFoundry.StatusManagement;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.ApplicationProcessor
{
    public interface IStatusManagementService
    {
        Task ChangeStatus( string entityId, string newStatus, List<string> reasons);

        Task<IEnumerable<IActivity>> GetActivities(string applicationNumber);

        Task RejectApplication(string entityId, List<string> reasons);

        Task ApplicationStatusMove(string applicationNumber);

        Task ReviveApplication(string applicationNumber);

        Task FinalOfferReadyFromManualReview(string applicationNumber, object offerId);
    }
}
