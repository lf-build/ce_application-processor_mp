﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.ApplicationProcessor
{
    public class ApplicationExtension : IApplicationExtension
    {
        [JsonConverter(typeof(InterfaceConverter<IMobileVerificationRequest, MobileVerificationRequest>))]
        public IMobileVerificationRequest MobileVerificationDetail { get; set; }
        public string EmailVerificationResult { get; set; }

        public string EmailVerificationProvider { get; set; }
    }
}
