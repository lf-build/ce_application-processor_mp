﻿namespace CreditExchange.ApplicationProcessor
{
    public enum BureauFileType
    {

        Thick = 1,
        Thin = 2,
        NTC = 3,
        NotFound = 4

    }
}
