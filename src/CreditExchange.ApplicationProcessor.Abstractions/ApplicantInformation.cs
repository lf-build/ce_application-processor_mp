﻿using System;
using System.Collections.Generic;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Date;

namespace CreditExchange.ApplicationProcessor
{
    public class ApplicantInformation : IApplicantInformation
    {
        public int Age { get; set; }
        public string City { get; set; }
        public double Income { get; set; }
        public double RequestedAmount { get; set; }
        public string EmpType { get; set; }
        public string Pan { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public double SelfEmi { get; set; }
        public double CreditCardBalance { get; set; }
        public string PurposeOfLoan { get; set; }
        //string ZipcodeRisk { get; set; }

        //string ZipcodeZone { get; set; }
        public Gender Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string ResidenceType { get; set; }

        public string ZipCode { get; set; }

        public DateTimeOffset DateOfBirth { get; set; }
        public TimeBucket ApplicationDate { get; set; }

        public string DrivingLicNo { get; set; }
        public string LandlineNo { get; set; }
        public string MobileNo { get; set; }

        public List<Address> Addresses { get; set; }
        public string CinNumber { get; set; }
        public string CompanyEmailAddress { get; set; }
        public string CompanyName { get; set; }
        public string PersonalEmail { get; set; }
        public string AadhaarNumber { get; set; }
        public List<Address> employmentAddress { get; set; }
    }
}
