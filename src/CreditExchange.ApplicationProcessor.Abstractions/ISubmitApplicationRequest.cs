﻿using System;
using CreditExchange.Applicant;
using CreditExchange.Application;
using LendFoundry.Foundation.Date;

namespace CreditExchange.ApplicationProcessor {
    public interface ISubmitApplicationRequest {
        string PurposeOfLoan { get; set; }
        double RequestedAmount { get; set; }
        LoanFrequency RequestedTermType { get; set; }
        double RequestedTermValue { get; set; }
        string AadhaarNumber { get; set; }
        DateTimeOffset DateOfBirth { get; set; }
        string Salutation { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        Gender Gender { get; set; }
        MaritalStatus MaritalStatus { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string UserId { get; set; }
        string PermanentAccountNumber { get; set; }
        string PersonalMobile { get; set; }
        bool IsMobileVerified { get; set; }
        DateTimeOffset? MobileVerificationTime { get; set; }
        string MobileVerificationNotes { get; set; }
        string PersonalEmail { get; set; }
        string ResidenceType { get; set; }
        string EducationalInstitution { get; set; }
        string LevelOfEducation { get; set; }
        string CurrentAddressLine1 { get; set; }
        string CurrentAddressLine2 { get; set; }
        string CurrentAddressLine3 { get; set; }
        string CurrentAddressLine4 { get; set; }
        string CurrentCity { get; set; }
        string CurrentCountry { get; set; }
        string CurrentLandMark { get; set; }
        string CurrentLocation { get; set; }
        string CurrentPinCode { get; set; }
        string CurrentState { get; set; }
        string PermanentAddressLine1 { get; set; }
        string PermanentAddressLine2 { get; set; }
        string PermanentAddressLine3 { get; set; }
        string PermanentAddressLine4 { get; set; }
        string PermanentCity { get; set; }
        string PermanentCountry { get; set; }
        string PermanenttLandMark { get; set; }
        string PermanentLocation { get; set; }
        string PermanentPinCode { get; set; }
        string PermanentState { get; set; }
        TimeBucket EmploymentAsOfDate { get; set; }
        string CinNumber { get; set; }
        string Designation { get; set; }
        EmploymentStatus EmploymentStatus { get; set; }
        int LengthOfEmploymentInMonths { get; set; }
        string EmployerName { get; set; }
        string WorkEmail { get; set; }
        double Income { get; set; }
        PaymentFrequency PaymentFrequency { get; set; }
        string WorkMobile { get; set; }
        string WorkAddressLine1 { get; set; }
        string WorkAddressLine2 { get; set; }
        string WorkAddressLine3 { get; set; }
        string WorkAddressLine4 { get; set; }
        string WorkCity { get; set; }
        string WorkCountry { get; set; }
        string WorkLandMark { get; set; }
        string WorkLocation { get; set; }
        string WorkPinCode { get; set; }
        string WorkState { get; set; }
        double CreditCardBalances { get; set; }
        double DebtPayments { get; set; }
        double MonthlyExpenses { get; set; }
        double MonthlyRent { get; set; }
        string SourceReferenceId { get; set; }
        SourceType SourceType { get; set; }
        SystemChannel SystemChannel { get; set; }
        string TrackingCode { get; set; }
        string OtherPurposeDescription { get; set; }

        //string BankName { get; set; }
        //string BankAccountHolderName { get; set; }
        //string BankAccountNumber { get; set; }
        //AccountType BankAccountType { get; set; }
        //string BankIfscCode { get; set; }
        string PromoCode { get; set; }
        string SodexoCode { get; set; }

        string TrackingCodeMedium { get; set; }
        string GCLId { get; set; }
        string CurrentAddressYear { get; set; }
        string CurrentAddressMonth { get; set; }
        string EmployementYear { get; set; }
        string EmployementMonth { get; set; }
        string ProductId { get; set; }
        string TrackingCodeCampaign { get; set; }
        string TrackingCodeTerm { get; set; }
        string TrackingCodeContent { get; set; }
    }
}