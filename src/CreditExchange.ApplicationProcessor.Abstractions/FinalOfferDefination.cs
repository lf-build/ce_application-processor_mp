﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using LendFoundry.Foundation.Date;
using System;

namespace CreditExchange.ApplicationProcessor
{
    public class FinalOfferDefination : Aggregate , IFinalOfferDefination
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public ScoreCardApplicationStatus Status { get; set; }

        public List<string> Reasons { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IFinalOffer, FinalOffer>))]
        public List<IFinalOffer> FinalOffers { get; set; }
        public TimeBucket GeneratedOn { get; set; }
        public DateTimeOffset OfferSelectionDate { get; set; }

        public bool IsAvailable { get; set; }
    }
}
