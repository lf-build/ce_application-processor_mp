﻿using LendFoundry.Application.Document;
using LendFoundry.ProductRule;

namespace CreditExchange.ApplicationProcessor {
    public class ApplicationProcessorConfiguration {

        public string ReviveApplicationRuleName { get; set; }
        public CaseInsensitiveDictionary<string> Statuses { get; set; }
        public CaseInsensitiveDictionary<RuleDetail> Rules { get; set; }
        public string[] ReApplyStatuseCodes { get; set; }
        public string[] RestartApplyStatusCodes { get; set; }
        public int ReApplyTimeFrame { get; set; }
        public bool EnablePersonalEmailVerification { get; set; }
        public string EmailValidationProvider { get; set; }
        public string PrimaryCreditBureau { get; set; }
        public string SecondaryCreditBureau { get; set; }
        public string[] EmploymentChangeStatus { get; set; }
        public string[] BankChangeStatus { get; set; }
        public string[] ExpenseChangeStatus { get; set; }
        public string[] AddressChangeStatus { get; set; }
        public string[] UpdatePersonalDetails { get; set; }
        public bool EnableEmail { get; set; }
        public int DefaultLoanTerm { get; set; }
        public int DefaultPaymentFrequency { get; set; }
        public double MinFinalOfferAmount { get; set; } = 50000;
        public double MaxFinalOfferAmount { get; set; } = 750000;

        public AppDocumentAttrib DrfConfiguration { get; set; }
        public AppDocumentAttrib DpnConfiguration { get; set; }

        public string[] EmailBlackList { get; set; }

        public string DefaultProductId { get; set; }
    }
}