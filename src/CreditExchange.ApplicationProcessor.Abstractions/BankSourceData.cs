﻿namespace CreditExchange.ApplicationProcessor
{
    public class BankSourceData : IBankSourceData
    {
      
        public string BankSourceType { get; set; }

        public object BankReport { get; set; }

    }
}
