﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.ApplicationProcessor
{
    public interface IInitialOfferEngineRepository : IRepository<IInitialOfferDefination>
    {
        Task<IInitialOfferDefination> GetInitialOffers(string entityType, string entityId);
        Task<IInitialOffer> SetSelectedInitialOffer(string entityType, string entityId, string offerId, DateTimeOffset selectionDate);
        Task AddInitialOffer(string entityType, string entityId, IInitialOffer offer);
        Task<IInitialOfferDefination> AddOffer(string entityType, string entityId, IInitialOfferDefination offer);
    }
}