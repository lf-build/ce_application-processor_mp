using System;

namespace CreditExchange.ApplicationProcessor
{
    
    public class BankStatementUploadFailedException : Exception
    {
        public  BankStatementUploadFailedException()
        {
        }
        public BankStatementUploadFailedException(string message) : base(message)
        {
          
        }

       
    }
}