﻿using System.Threading.Tasks;

namespace CreditExchange.ApplicationProcessor {
    public interface IOrchestrationService {
        Task<IInitialOfferDefination> RunInitialOffer (string applicationNumber, bool isReCompute);
        Task<IFinalOfferDefination> RunFinalOffer (string applicationNumber, bool isReCompute);
        Task<IInitialOfferDefination> RunInitialOffer (string applicationNumber, bool isReCompute, string productId);
        Task<IFinalOfferDefination> RunFinalOffer (string applicationNumber, bool isReCompute, string productId, string workflowStatusId);

        Task<bool> RepullCibil(string applicationNumber, string productId);
    }
}