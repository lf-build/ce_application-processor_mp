﻿namespace CreditExchange.ApplicationProcessor
{
    public class FinalOffer : IFinalOffer
    {
        public string OfferId { get; set; }

        public string Loantenure { get; set; }

        public double OfferAmount { get; set; }

        public double Emi { get; set; }
        public double FinalOfferAmount { get; set; }
        public double NetMonthlyIncome { get; set; }
        public double CurrentEMINMI { get; set; }
        public double MaxPermittedEMI { get; set; }
        public int ProcessingFee { get; set; }
        public double ProcessingFeePercentage { get; set; }
        public double CalculatedProcessingFee { get; set; }
        public double InterestRate { get; set; }

        public bool ManualReview { get; set; }

        public bool IsSelected { get; set; }

        public string RejectCode { get; set; }

        public bool IsManualOffer { get; set; }
        public bool IsPresented { get; set; }

    }
}
