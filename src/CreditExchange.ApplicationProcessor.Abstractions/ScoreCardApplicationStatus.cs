﻿namespace CreditExchange.ApplicationProcessor
{
    public enum ScoreCardApplicationStatus
    {
        Initiated = 1,
        InProcess =2,
        Rejected=3,
        Failed=4,
        Completed=5,
        ManualReview = 6
    }
}
