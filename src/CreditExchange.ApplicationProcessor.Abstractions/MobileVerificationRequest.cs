﻿using System;

namespace CreditExchange.ApplicationProcessor
{
    public class MobileVerificationRequest : IMobileVerificationRequest
    {
        public bool IsVerified { get; set; }
        public DateTimeOffset? VerificationTime { get; set; }
        public string Notes { get; set; }
    }
}
