﻿using System;

namespace CreditExchange.ApplicationProcessor
{
    public class PerfiosReportData: IPerfiosReportData
    {
        public double MonthlyEmi { get; set; }
        public double MonthlyExpense { get; set; }
        public double MonthlyIncome { get; set; }
        public double AvgInvestmentExpensePerMonth { get; set; }
        public double AvgDailyBalance { get; set; }
        public DateTimeOffset SalaryDate { get; set; }
        public double EcsBounce { get; set; }
        public string ReferenceNumber { get; set; }
            
    }
}
