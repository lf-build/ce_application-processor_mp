﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.ApplicationProcessor
{
    public class ManualFinalOfferAdded
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IFinalOffer,FinalOffer>))]
        public IFinalOffer ManualOffer { get; set; }
    }
}
