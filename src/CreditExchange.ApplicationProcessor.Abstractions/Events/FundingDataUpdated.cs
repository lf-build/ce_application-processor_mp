﻿namespace CreditExchange.ApplicationProcessor.Events
{
    public class FundingDataUpdated
    {
      public  string EntityType { get; set; }
        public string EntityId { get; set; }
        public string ReferenceNumber { get; set; }
        public object Request { get; set; }
        public object Response { get; set; }
    }
}
