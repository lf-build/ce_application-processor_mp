﻿namespace CreditExchange.ApplicationProcessor.Events
{
    public class ApplicationCreatedWithLeadStatus
    {
        public string ApplicationNumber { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
    }
}
