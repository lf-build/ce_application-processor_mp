﻿
namespace CreditExchange.ApplicationProcessor
{
    public class FinalOfferSelected
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        //public string OfferId { get; set; }
        public bool IsAccepted { get; set; }
        public IFinalOffer SelectedFinalOffer { get; set; }
        public object Applicaiton { get; set; }
    }
}