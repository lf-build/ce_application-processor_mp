﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.ApplicationProcessor
{
    public class ManualInitialOfferAdded
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IInitialOffer, InitialOffer>))]
        public IInitialOffer ManualOffer { get; set; }
    }
}
