﻿namespace CreditExchange.ApplicationProcessor
{
    public enum Religion
    {
        Hindu =1,
        Muslim = 2,
        Christian = 3,
        Sikh = 4 ,
        Zoroastrian = 5 ,
        Jain = 6,
        Other = 7
    }
}
