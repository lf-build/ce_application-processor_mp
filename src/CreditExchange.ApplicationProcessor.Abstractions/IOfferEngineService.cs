﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.ApplicationProcessor
{
    public interface IOfferEngineService
    {
        Task<IInitialOfferDefination> InitiateInitialOffer(string applicationNumber,bool isReCallSyndication);
        Task<IFinalOfferDefination> InitiateFinalOffer(string applicationNumber, bool isReCallSyndication);
        Task<IFinalOfferDefination> GetFinalOffer(string applicationNumber, string filter = null);
        Task<IInitialOfferDefination> GetInitialOffer(string applicationNumber);
        Task RejectInitialOffer(string applicationNumber, List<string> reasons);
        Task SelectInitialOffer(string applicationNumber, string offerId);
        Task RejectFinalOffer(string applicationNumber, List<string> reasons);
        Task SelectFinalOffer(string applicationNumber, string offerId);
       // Task SetSelectedFinalOffer(string applicationNumber, string offerId, bool isAccepted, List<string> reasons);
        Task UpdateCashFlow(string applicationNumber, object value);
        Task AddInitialOffer(string applicationNumber, IInitialOffer initialOffer);
        Task AddFinalOffer(string applicationNumber, IFinalOffer finalOffer);
        Task UpdateFinalOfferIntermediateData(string applicationnumber, string grade, string score);
        Task RepresentManualOffer(string applicationNumber, IFinalOffer finalOffer);

        Task<bool> RepullCibil(string applicationNumber, string productId);
        
    }
}
