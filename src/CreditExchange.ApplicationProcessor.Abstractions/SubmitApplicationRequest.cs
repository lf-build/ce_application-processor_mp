﻿using System;
using CreditExchange.Applicant;
using CreditExchange.Application;
using LendFoundry.Foundation.Date;

namespace CreditExchange.ApplicationProcessor {
    public class SubmitApplicationRequest : ISubmitApplicationRequest {
        /// <summary>
        /// default constructor
        /// </summary>
        public SubmitApplicationRequest () {

        }
        /// <summary>
        ///for partial update application endpoints 
        /// </summary>
        /// <param name="request">update fields request + existing details</param>
        public SubmitApplicationRequest (SubmitApplicationRequest request) {
            PurposeOfLoan = request.PurposeOfLoan;
            RequestedAmount = request.RequestedAmount;
            RequestedTermType = request.RequestedTermType;
            RequestedTermValue = request.RequestedTermValue;
            AadhaarNumber = request.AadhaarNumber;
            DateOfBirth = request.DateOfBirth;
            Salutation = request.Salutation;
            FirstName = request.FirstName;
            MiddleName = request.MiddleName;
            LastName = request.LastName;
            Gender = request.Gender;
            MaritalStatus = request.MaritalStatus;
            UserName = request.UserName;
            Password = request.Password;
            UserId = request.UserId;
            PermanentAccountNumber = request.PermanentAccountNumber;
            PersonalMobile = request.PersonalMobile;
            IsMobileVerified = request.IsMobileVerified;
            MobileVerificationTime = request.MobileVerificationTime;
            MobileVerificationNotes = request.MobileVerificationNotes;
            PersonalEmail = request.PersonalEmail;
            ResidenceType = request.ResidenceType;
            EducationalInstitution = request.EducationalInstitution;
            LevelOfEducation = request.LevelOfEducation;
            CurrentAddressLine1 = request.CurrentAddressLine1;
            CurrentAddressLine2 = request.CurrentAddressLine2;
            CurrentAddressLine3 = request.CurrentAddressLine3;
            CurrentAddressLine4 = request.CurrentAddressLine4;
            CurrentCity = request.CurrentCity;
            CurrentCountry = request.CurrentCountry;
            CurrentLandMark = request.CurrentLandMark;
            CurrentLocation = request.CurrentLocation;
            CurrentPinCode = request.CurrentPinCode;
            CurrentState = request.CurrentState;
            PermanentAddressLine1 = request.PermanentAddressLine1;
            PermanentAddressLine2 = request.PermanentAddressLine2;
            PermanentAddressLine3 = request.PermanentAddressLine3;
            PermanentAddressLine4 = request.PermanentAddressLine4;
            PermanentCity = request.PermanentCity;
            PermanentCountry = request.PermanentCountry;
            PermanenttLandMark = request.PermanenttLandMark;
            PermanentLocation = request.PermanentLocation;
            PermanentPinCode = request.PermanentPinCode;
            PermanentState = request.PermanentState;
            EmploymentAsOfDate = request.EmploymentAsOfDate;
            CinNumber = request.CinNumber;
            Designation = request.Designation;
            EmploymentStatus = request.EmploymentStatus;
            LengthOfEmploymentInMonths = request.LengthOfEmploymentInMonths;
            EmployerName = request.EmployerName;
            WorkEmail = request.WorkEmail;
            Income = request.Income;
            PaymentFrequency = request.PaymentFrequency;
            WorkMobile = request.WorkMobile;
            WorkAddressLine1 = request.WorkAddressLine1;
            WorkAddressLine2 = request.WorkAddressLine2;
            WorkAddressLine3 = request.WorkAddressLine3;
            WorkAddressLine4 = request.WorkAddressLine4;
            WorkCity = request.WorkCity;
            WorkCountry = request.WorkCountry;
            WorkLandMark = request.WorkLandMark;
            WorkLocation = request.WorkLocation;
            WorkPinCode = request.WorkPinCode;
            WorkState = request.WorkState;
            CreditCardBalances = request.CreditCardBalances;
            DebtPayments = request.DebtPayments;
            MonthlyExpenses = request.MonthlyExpenses;
            MonthlyRent = request.MonthlyRent;
            SourceReferenceId = request.SourceReferenceId;
            SourceType = request.SourceType;
            SystemChannel = request.SystemChannel;
            TrackingCode = request.TrackingCode;
            OtherPurposeDescription = request.OtherPurposeDescription;
            BankName = request.BankName;
            BankAccountHolderName = request.BankAccountHolderName;
            BankAccountNumber = request.BankAccountNumber;
            BankAccountType = request.BankAccountType;
            BankIfscCode = request.BankIfscCode;
            PromoCode = request.PromoCode;
            SodexoCode = request.SodexoCode;
            TrackingCodeMedium = request.TrackingCodeMedium;
            GCLId = request.GCLId;
            CurrentAddressYear = request.CurrentAddressYear;
            CurrentAddressMonth = request.CurrentAddressMonth;
            EmployementMonth = request.EmployementMonth;
            EmployementYear = request.EmployementYear;
            TrackingCodeCampaign = request.TrackingCodeCampaign;
            TrackingCodeTerm = request.TrackingCodeTerm;
            TrackingCodeContent = request.TrackingCodeContent;
        }
        //[JsonConverter(typeof(InterfaceConverter<IMobileVerificationRequest, MobileVerificationRequest>))]
        //public IMobileVerificationRequest MobileVerificationDetail { get; set; }
        public string PurposeOfLoan { get; set; }
        public double RequestedAmount { get; set; }
        public LoanFrequency RequestedTermType { get; set; }
        public double RequestedTermValue { get; set; }
        public string AadhaarNumber { get; set; }
        public DateTimeOffset DateOfBirth { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserId { get; set; }
        public string PermanentAccountNumber { get; set; }
        public string PersonalMobile { get; set; }
        public bool IsMobileVerified { get; set; }
        public DateTimeOffset? MobileVerificationTime { get; set; }
        public string MobileVerificationNotes { get; set; }
        public string PersonalEmail { get; set; }
        public string ResidenceType { get; set; }
        public string EducationalInstitution { get; set; }
        public string LevelOfEducation { get; set; }
        public string CurrentAddressLine1 { get; set; }
        public string CurrentAddressLine2 { get; set; }
        public string CurrentAddressLine3 { get; set; }
        public string CurrentAddressLine4 { get; set; }
        public string CurrentCity { get; set; }
        public string CurrentCountry { get; set; }
        public string CurrentLandMark { get; set; }
        public string CurrentLocation { get; set; }
        public string CurrentPinCode { get; set; }
        public string CurrentState { get; set; }
        public string PermanentAddressLine1 { get; set; }
        public string PermanentAddressLine2 { get; set; }
        public string PermanentAddressLine3 { get; set; }
        public string PermanentAddressLine4 { get; set; }
        public string PermanentCity { get; set; }
        public string PermanentCountry { get; set; }
        public string PermanenttLandMark { get; set; }
        public string PermanentLocation { get; set; }
        public string PermanentPinCode { get; set; }
        public string PermanentState { get; set; }
        public TimeBucket EmploymentAsOfDate { get; set; }
        public string CinNumber { get; set; }
        public string Designation { get; set; }
        public EmploymentStatus EmploymentStatus { get; set; }
        public int LengthOfEmploymentInMonths { get; set; }
        public string EmployerName { get; set; }
        public string WorkEmail { get; set; }
        public double Income { get; set; }
        public Applicant.PaymentFrequency PaymentFrequency { get; set; }
        public string WorkMobile { get; set; }
        public string WorkAddressLine1 { get; set; }
        public string WorkAddressLine2 { get; set; }
        public string WorkAddressLine3 { get; set; }
        public string WorkAddressLine4 { get; set; }
        public string WorkCity { get; set; }
        public string WorkCountry { get; set; }
        public string WorkLandMark { get; set; }
        public string WorkLocation { get; set; }
        public string WorkPinCode { get; set; }
        public string WorkState { get; set; }
        public double CreditCardBalances { get; set; }
        public double DebtPayments { get; set; }
        public double MonthlyExpenses { get; set; }
        public double MonthlyRent { get; set; }
        public string SourceReferenceId { get; set; }
        public SourceType SourceType { get; set; }
        public SystemChannel SystemChannel { get; set; }
        public string TrackingCode { get; set; }
        public string OtherPurposeDescription { get; set; }
        public string BankName { get; set; }
        public string BankAccountHolderName { get; set; }
        public string BankAccountNumber { get; set; }
        public AccountType BankAccountType { get; set; }
        public string BankIfscCode { get; set; }
        public string PromoCode { get; set; }
        public string SodexoCode { get; set; }
        public string TrackingCodeMedium { get; set; }
        public string GCLId { get; set; }
        public string CurrentAddressYear { get; set; }
        public string CurrentAddressMonth { get; set; }
        public string EmployementYear { get; set; }
        public string EmployementMonth { get; set; }
        public string ProductId { get; set; }
        public string TrackingCodeCampaign { get; set; }
        public string TrackingCodeTerm { get; set; }
        public string TrackingCodeContent { get; set; }
    }
}