﻿using System;

namespace CreditExchange.ApplicationProcessor
{
    public interface IMobileVerificationRequest
    {
        bool IsVerified { get; set; }
        DateTimeOffset? VerificationTime { get; set; }
        string Notes { get; set; }
    }
}
