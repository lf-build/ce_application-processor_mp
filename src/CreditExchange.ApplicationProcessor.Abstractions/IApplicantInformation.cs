﻿using System;
using System.Collections.Generic;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Date;

namespace CreditExchange.ApplicationProcessor
{
    public interface IApplicantInformation
    {
        int Age { get; set; }
        string City { get; set; }
        double Income { get; set; }
        double RequestedAmount { get; set; }
        string EmpType { get; set; }
        string Pan { get; set; }
        string FirstName { get; set; }

        string MiddleName { get; set; }
        string LastName { get; set; }
        double SelfEmi { get; set; }
        double CreditCardBalance { get; set; }
        string PurposeOfLoan { get; set; }
        //string ZipcodeRisk { get; set; }

        //string ZipcodeZone { get; set; }
        Gender Gender { get; set; }
        string MaritalStatus { get; set; }
        string ResidenceType { get; set; }

        string ZipCode { get; set; }

        DateTimeOffset DateOfBirth { get; set; }
        TimeBucket ApplicationDate { get; set; }

        string DrivingLicNo { get; set; }
        string LandlineNo { get; set; }
        string MobileNo { get; set; }
        
        List<Address> Addresses { get; set; }

        string CinNumber { get; set; }

        string CompanyEmailAddress { get; set; }

        string CompanyName { get; set; }

        string PersonalEmail { get; set; }

        string AadhaarNumber { get; set; }

        List<Address> employmentAddress { get; set; }
    }
}
