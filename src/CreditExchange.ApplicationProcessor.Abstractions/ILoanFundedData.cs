﻿using System;

namespace CreditExchange.ApplicationProcessor
{
   public interface ILoanFundedData
    {
        DateTimeOffset FundedDate { get; set; }
        double FundedAmount { get; set; }
        DateTimeOffset FirstEmiDate { get; set; }
        double EmiAmount { get; set; }
        string LoanAccountNumber { get; set; }
        string PaymentStatus { get; set; }
        string UTRNumber { get; set; }
    }
}
