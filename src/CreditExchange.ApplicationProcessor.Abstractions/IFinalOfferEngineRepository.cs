﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace CreditExchange.ApplicationProcessor
{
    public interface IFinalOfferEngineRepository : IRepository<IFinalOfferDefination>
    {
        Task<IFinalOfferDefination> GetFinalOffers(string entityType, string entityId, string filter = null);

        Task<IFinalOffer> SetSelectedFinalOffer(string entityType, string entityId, string offerId,DateTimeOffset selectedDate);

        Task AddFinalOffer(string entityType, string entityId, IFinalOffer offer);

        Task<IFinalOfferDefination> AddOffer(string entityType, string entityId, IFinalOfferDefination offer);

       
    }
}