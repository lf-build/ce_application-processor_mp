using System;

namespace CreditExchange.ApplicationProcessor
{
    public class OfferGenerationFailedException : Exception
    {
        public OfferGenerationFailedException(string message) : base(message)
        {

        }
    }
}