﻿namespace CreditExchange.ApplicationProcessor
{
    public class InitialOffer : IInitialOffer
    {
        public string OfferId { get; set; }
        public string FileType { get; set; }
        //public decimal InitialScore { get; set; }
        public double InterestRate { get; set; }
        public int LoanTenure { get; set; }
        public double Score { get; set; }
        public double OfferAmount { get; set; }
        public double FinalOfferAmount { get; set; }
        public double MaxPermittedEMI { get; set; }
        public double netMonthlyIncome { get; set; }
        public double currentEMINMI { get; set; }
        public double monthlyExpense { get; set; }
        
        public bool IsSelected { get; set; }

        public string RejectCode { get; set; }

        public bool IsManualOffer { get; set; }
    }
}
