﻿using System;

namespace CreditExchange.ApplicationProcessor
{
    public class LoanFundedData : ILoanFundedData
    {
        public DateTimeOffset FundedDate { get; set; }
        public double FundedAmount { get; set; }
        public DateTimeOffset FirstEmiDate { get; set; }
        public double EmiAmount { get; set; }
        public string LoanAccountNumber { get; set; }
        public string PaymentStatus { get; set; }
        public string UTRNumber { get; set; }
    }
}
